<style type="text/css">
	.panel {
		margin-top: 50;
	}
</style>

<script type="text/javascript">
	$(document).ready(function(){
		$.toast({
		    heading: 'Acesso Negado!',
		    text: 'Sem permissão de acesso.',
		    showHideTransition: 'fade',
		    position: 'top-right',
		    icon: 'warning'
		});
	});
</script>

<div class="panel panel-warning">
	<div class="panel-heading"> 
		<h3 class="panel-title"><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> Acesso Negado!</h3> 
	</div>
	<div class="panel-body">
		<strong>Acesso Negado!</strong> Sem permissão de acesso. <br>
		<li><strong>Seu Grupo Atual:</strong> <?php echo $detalhes->descricao_grupo; ?> </li>
		<li><strong>Detalhes desta tela :</strong> <?php echo $detalhes->descricao_aplicacao; ?> </li>
		<a href="<?php echo base_url(); ?>">Voltar</a>
	</div>
</div>