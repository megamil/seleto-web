
    <style>
        @media screen and (min-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: left;
            }

            #filtros {
                display: table;
            }

            #filtros_p {
                display: none;
            }

            #filtros_p_topo {
                display: none;
            }

            #login_cliente_filtro_enviar_grande {
                width: 70%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar_grande:hover {
                cursor: pointer;
                opacity: .60;
            }
        }

        @media screen and (max-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: center;
            }

            #filtros {
                display: none;
            }

            #filtros_p {
                display: table;
                margin-left: 0;
                background-color: #b4b4b4;
                width: 100%;
                height: 2%;
                float: left;
            }

            #login_cliente_filtro {
                width: 100%;
            }

            #cor_filtro_pequeno {
                background: #b4b4b4;
                border: none;
            }

            #login_cliente_filtro_enviar {
                width: 80%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar:hover {
                cursor: pointer;
                opacity: .60;
            }
        }
    </style>
<!-- filtro topo grande -->
<div class="col-lg-4" id="filtros"
     style=" margin-left: 0; background-color: #b4b4b4; width: 15%; height: 100%; float: left;">
    <p style="margin-top: 15%; margin-left: 3%"><b><font size="5px">FILTRO</font> </b></p>
    <form style="margin-left: 3%">
        <p><b>Nome</b></p>
        <input type="text" class="login_cliente" style="width: 100%; height: 30px">

        <p><b>CPF</b></p>
        <input type="text" class="login_cliente" style="width: 100%; height: 30px">


        <p style="margin-top: 10px"><b>Categoria</b></p>
        <select class="login_cliente" style="width: 100%; height: 30px">
            <option>Premium</option>
            <option>Taxi</option>
            <option>Ultra econômico</option>
            <option>Econômico</option>
        </select>


        <p style="margin-top: 10px"><b>Placa</b></p>
        <input class="login_cliente" style="width: 100%; height: 30px">

        <p style="margin-top: 10px">Status</p>
        <select class="login_cliente" style="width: 100%; height: 30px; margin-bottom: 10%">
            <option>Ativo</option>
            <option>Bloqueado</option>
            <option>Inativo</option>
            <option>Vistoria</option>
            <option>Reprovado</option>
        </select>
        <input type="submit" value="Filtrar" class="login_cliente" id="login_cliente_filtro_enviar_grande">
    </form>
</div>
<!-- começa o filtro para tela pequena e termina a grande -->
<div class="navbar-wrapper" id="filtros_p_topo">
    <nav class="navbar navbar-inverse navbar-static-top" id="cor_filtro_pequeno">
        <div class="container">
            <div class="navbar-header">
                <button type="button" id="dp" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar_filtro" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="esp_logo"></div>
                <a class="navbar-brand" href="#"></a>
                <h3>Filtros</h3>
            </div>
            <div id="navbar_filtro" class="navbar-collapse collapse">
                <div class="col-lg-4" class="navbar-toggle" id="filtros_p">
                    <form style="margin-left: 3%" action="#" method="POST">
                        <b>Nome</b>
                        <input class="login_cliente" id="login_cliente_filtro">


                        <b>Categoria</b>
                        <input class="login_cliente" id="login_cliente_filtro">


                        <b>Placa</b>
                        <input type="text" class="login_cliente" id="login_cliente_filtro">
                        <p>&nbsp;</p>
                        <input type="submit" class="login_cliente" id="login_cliente_filtro_enviar">
                    </form>

                </div>
            </div>
        </div>
    </nav>
</div>


<!-- abre caixa com o clinte -->

<div style="margin-bottom: 50px">
    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
</div>
<div style="background-color: #e5e5e5;  height: 100px; width:80% ; margin-top: 20px;margin-left: 20%; display: table;">
    <div style="margin-left: 20px; margin-right: 20px; padding-top: 2%">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Categoria</th>
                <th>Placa</th>
                <th>Veículo</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                <th scope="row">#5</th>
                <td>Rafael de Lima</td>
                <td>Premium</td>
                <td>ACK-5214</td>
                <td>Palio</td>
                <td>Ativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_motorista.php'">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>


            <tr>
                <th scope="row">#10</th>
                <td>Caio de Lima</td>
                <td>Taxi</td>
                <td>KSD-9854</td>
                <td>Gol</td>
                <td>Ativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_motorista.php'">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>
            <tr>
                <th scope="row">#1</th>
                <td>Daniel Teixeira</td>
                <td>Premium</td>
                <td>KAJ-9851</td>
                <td>Uno</td>
                <td>Bloqueado</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_motorista.php'">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>

            <tr>
                <th scope="row">#99</th>
                <td>Marcos Yuri</td>
                <td>Ultra Econômico</td>
                <td>OQK-9854</td>
                <td>Uno</td>
                <td>Inativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>

            <tr>
                <th scope="row">#17</th>
                <td>Pedro Barbosa</td>
                <td>Econômico</td>
                <td>OEK-9854</td>
                <td>Sandero</td>
                <td>Bloqueado</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>

            </tr>
            <tr>
                <th scope="row">#112</th>
                <td>Alexandre Yuri</td>
                <td>Ultra Econômico</td>
                <td>OSK-8541</td>
                <td>Gol</td>
                <td>Ativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>

            <tr>
                <th scope="row">#78</th>
                <td>André Marcos</td>
                <td>Premium</td>
                <td>IJS-8547</td>
                <td>Sandero</td>
                <td>Inativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>

            <tr>
                <th scope="row">#112</th>
                <td>Alexandre Yuri</td>
                <td>Ultra Econômico</td>
                <td>OSK-8541</td>
                <td>Gol</td>
                <td>Ativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>

            <tr>
                <th scope="row">#78</th>
                <td>André Marcos</td>
                <td>Premium</td>
                <td>IJS-8547</td>
                <td>Sandero</td>
                <td>Inativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>

            <tr>
                <th scope="row">#10</th>
                <td>Caio de Lima</td>
                <td>Taxi</td>
                <td>KSD-9854</td>
                <td>Gol</td>
                <td>Ativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>

            <tr>
                <th scope="row">#5</th>
                <td>Rafael de Lima</td>
                <td>Premium</td>
                <td>ACK-5214</td>
                <td>Palio</td>
                <td>Ativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>

            <tr>
                <th scope="row">#78</th>
                <td>André Marcos</td>
                <td>Premium</td>
                <td>IJS-8547</td>
                <td>Sandero</td>
                <td>Inativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>

            <tr>
                <th scope="row">#5</th>
                <td>Rafael de Lima</td>
                <td>Premium</td>
                <td>ACK-5214</td>
                <td>Palio</td>
                <td>Ativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>

            <tr>
                <th scope="row">#78</th>
                <td>André Marcos</td>
                <td>Premium</td>
                <td>IJS-8547</td>
                <td>Sandero</td>
                <td>Inativo</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner">Bloquear</button>
                    <button type="submit" class="btn btn-success formulario-banner">Aprovar</button>
                </td>


            </tr>

            </tbody>
        </table>

    </div>
</div>
