<head>
    <link rel="shortcut icon" href="<?php echo base_url() ?>style/imagens/favicon.png">
    <link href="<?php echo base_url(); ?>style/css/bootstrap.css" rel="stylesheet">

     <link type="text/css" href="<?php echo base_url(); ?>style/css/select2.css" rel="stylesheet" />
    <link type="text/css" href="<?php echo base_url(); ?>style/css/jquery.toast.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/sweetalert.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.mask.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/script.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.toast.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/select2.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/sweetalert.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/moment.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.priceformat.js"></script>
    
    <title>Seleto | DashBoard</title>
</head>


<div style="background-color: #003496;">


    <div class="navbar-wrapper" id="topo" style="background: #3c3c3c;">
        <div id="topo_cor" style="background: #3c3c3c;">
            <nav class="navbar navbar-inverse navbar-static-top" id="topo_bk_none" style="background: #3c3c3c;">
                <div class="container" id="aumentar_topo" style="background: #3c3c3c;">
                    <div class="navbar-header" id="topoo" style="background: #3c3c3c;">
                        <button type="button" id="dp" class="navbar-toggle collapsed " data-toggle="collapse"
                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div id="esp_logo"></div>
                        <a class="navbar-brand" href="<?php echo base_url(); ?>main/login"><img src="<?php echo base_url(); ?>style/imagens/seleto_menu.png" id="logotipo" style="margin-top: -6%"></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse" style="background: #3c3c3c;">
                        <div id="topo_cima_tudo">

                            <ul class="nav navbar-nav " id="mover_topo_icones" style="margin-left: 18%">

                                <li><a href="<?php echo base_url(); ?>main/redirecionar/17" id="icone_rede_menu"><img src="<?php echo base_url(); ?>style/imagens/recebiveis1.png" style="width: 140px;margin-top: -3px;"></a></li>
                                <li><a href="<?php echo base_url(); ?>main/redirecionar/23" id="icone_rede_menu"><img src="<?php echo base_url(); ?>style/imagens/corridas1.png" style="width: 120px;margin-left: -20px"></a></li>
                                <li><a href="<?php echo base_url(); ?>main/redirecionar/2" id="icone_rede_menu"><img src="<?php echo base_url(); ?>style/imagens/configuracoes1.png" style="width: 120px"></a></li>
                                <li><a href="<?php echo base_url(); ?>main/redirecionar/15" id="icone_rede_menu"><img src="<?php echo base_url(); ?>style/imagens/avisos_topo.png" style="width: 110px;margin-top: 2px"></a></li>



                                </li>


                                <!--
                                <li><a href="<?php echo base_url(); ?>main/redirecionar/cadastrar" id="icone_rede_menu" ><img src="<?php echo base_url(); ?>style/imagens/cadastrar1.png" style="width: 100px"></a></li>
                                -->

                                <li><a href="<?php echo base_url() ?>main/logout" id="icone_rede_menu"><img src="<?php echo base_url(); ?>style/imagens/sair.png"
                                                                                        style="width: 120px"></a>
                                </li>

                            </ul>
                        </div>
                        <li style="color:#fff;list-style: none;float: right;">Meus ganhos</li><br>
                        <li style="color:#fff;list-style: none;float: right;">R$ 874,00</li>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>style/js/vendor/jquery.min.js"><\/script>')</script>
<script src="<?php echo base_url(); ?>style/js/jq1.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="<?php echo base_url(); ?>style/js/jq3.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo base_url(); ?>style/js/jq2.js"></script>