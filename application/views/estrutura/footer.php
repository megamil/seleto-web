</div>


<footer class="footer">
  <div class="container">
  	<div class="row">
  		<div class="col-md-8">
  			<p class="text-muted">Rodapé.</p>	
  		</div>
  		<div class="col-md-4" align="right">
  			<small style="cursor: pointer;" id="notificar">
  				<span class="glyphicon glyphicon-music"></span> Ativar Notificações do navegador.
  			</small> 
  		</div>
  	</div>
  </div>
</footer>

</body>

</html>