<head>

    <link rel="shortcut icon" href="<?php echo base_url() ?>style/imagens/favicon.png">
    <link href="<?php echo base_url(); ?>style/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>style/css/menu.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.min.js"></script>

    <link type="text/css" href="<?php echo base_url(); ?>style/css/select2.css" rel="stylesheet" />
    <link type="text/css" href="<?php echo base_url(); ?>style/css/jquery.toast.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/css/sweetalert.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>style/jquery-ui/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.mask.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/script.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.toast.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/select2.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/sweetalert.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/moment.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.priceformat.js"></script>

    <title>Seleto | DashBoard</title>
</head>


<div style="background-color: #003496;">


    <div class="navbar-wrapper" id="topo">
        <div id="topo_cor">
            <nav class="navbar navbar-inverse navbar-static-top" id="topo_bk_none">
                <div class="container" id="aumentar_topo">
                    <div class="navbar-header" id="topoo">
                        <button type="button" id="dp" class="navbar-toggle collapsed " data-toggle="collapse"
                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div id="esp_logo"></div>
                        <a class="navbar-brand" href="<?php echo base_url() ?>main"><img src="<?php echo base_url(); ?>style/imagens/seleto_menu.png" id="logotipo" style="margin-top: -6%"></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <div id="topo_cima_tudo">
                            <ul class="nav navbar-nav " id="mover_topo_icones">

                                <li><a href="<?php echo base_url() ?>main/redirecionar/17" id="icone_rede_menu"><img src="<?php echo base_url(); ?>style/imagens/faturamento1.png"
                                                                                        style="width: 120px"></a></li>


                                <li><a href="<?php echo base_url() ?>main/redirecionar/18" id="icone_rede_menu"><img src="<?php echo base_url(); ?>style/imagens/clientes1.png"
                                                                                     style="width: 120px"></a></li>
                                <li><a href="<?php echo base_url() ?>main/redirecionar/20" id="icone_rede_menu"> <img src="<?php echo base_url(); ?>style/imagens/motoristas1.png"
                                                                                        style="width: 120px"></a></li>
                                <li class="dropdown" >
                                    <a href="#" id="icone_rede_menu" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-haspopup="true"
                                       aria-expanded="false"><font color="black"><img src="<?php echo base_url(); ?>style/imagens/corridas1.png"
                                                                                      style="width: 120px"> <span
                                                    class="caret"></span></a></font>
                                    <ul class="dropdown-menu">

                                        <!--Não sei pra onde vai --><li><a href="<?php echo base_url() ?>main/redirecionar/22"><font color="black">CORRIDAS</font></a></li>
                                        <!--Não sei pra onde vai --><li><a href="<?php echo base_url() ?>main/redirecionar/23"><font color="black">RELATORIOS</font></a>
                                        </li>


                                    </ul>
                                </li>


                                <li class="dropdown" >
                                    <a href="#" id="icone_rede_menu" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-haspopup="true"
                                       aria-expanded="false"><font color="black"><img src="<?php echo base_url(); ?>style/imagens/cadastrar1.png"
                                                                                      style="width: 120px"> <span
                                                    class="caret"></span></a></font>
                                    <ul class="dropdown-menu">

                                        <li><a href="<?php echo base_url() ?>main/redirecionar/19"><font color="black">CLIENTE</font></a></li>
                                        <li><a href="<?php echo base_url() ?>main/redirecionar/21"><font color="black">MOTORISTA</font></a>
                                        </li>


                                    </ul>
                                </li>
                                <li><!--class="active"--><a href="<?php echo base_url() ?>main/redirecionar/16" id="icone_rede_menu"><img
                                                src="<?php echo base_url(); ?>style/imagens/tarifas1.png" style="width: 120px"></a></li>
                                <li class="dropdown" >
                                    <a href="#" id="icone_rede_menu" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-haspopup="true"
                                       aria-expanded="false"><font color="black"><img src="<?php echo base_url(); ?>style/imagens/configuracoes1.png"
                                                                                      style="width: 120px"> <span
                                                    class="caret"></span></a></font>
                                    <ul class="dropdown-menu">

                          <li><a href="<?php echo base_url() ?>main/redirecionar/2"><font color="black">USUARIOS</font></a></li>
                                        </li>


                                    </ul>
                                </li>

                                <li><a href="<?php echo base_url() ?>main/redirecionar/15" id="icone_rede_menu"><p style="color: #fff; margin-top: 18%">Mensagens</p></li>
                                <!--
                                <li><a href="cadastrar.php" id="icone_rede_menu" ><img src="<?php echo base_url(); ?>style/imagens/cadastrar1.png" style="width: 100px"></a></li>
                                -->

                                <li><a href="<?php echo base_url() ?>main/logout" id="icone_rede_menu"><img src="<?php echo base_url(); ?>style/imagens/sair.png"
                                                                                             style="width: 120px"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>style/js/vendor/jquery.min.js"><\/script>')</script>
<script src="<?php echo base_url(); ?>style/js/jq1.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="<?php echo base_url(); ?>style/js/jq3.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo base_url(); ?>style/js/jq2.js"></script>

<style type="text/css">
    main{
        padding: 50px;
    }
</style>

<body style="background-image: url('<?php echo base_url() ?>style/imagens/backmaps.png');">

  <main>

    <div class="modal fade" id="modal_historico" tabindex="-1" role="dialog" aria-labelledby="modal_historicoLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modal_historicoLabel">Histórico de edições</h4>
          </div>
          <div class="modal-body">

            <div class="progress_modal_historico">
          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
            <span class="sr-only">50%</span>
          </div>
        </div>

            <div id="load_modal_historico"></div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">

  $(document).ready(function(){
    $('.progress_modal_historico').hide();

    $('#historico_modal').click(function () {
        
        $('.progress_modal_historico').show();
        var id = $(this).attr('cod');

        $('#load_modal_historico').load('<?php echo base_url() ?>Controller_relatorios/load_historico_edicoes',{id: id},function(){
          $('.progress_modal_historico').hide();

          dataTableLoad();

        });

    });

  });
</script>