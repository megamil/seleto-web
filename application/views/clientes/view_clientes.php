<head>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <link href="css/menu.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome-4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (!$.browser.webkit) {
                $('.wrapper').html('<p>Sorry! Non webkit users. :(</p>');
            }
        });
    </script>

    <style>
        @media screen and (min-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: left;
            }

            #filtros {
                display: table;
            }

            #filtros_p {
                display: none;
            }

            #filtros_p_topo {
                display: none;
            }

            #login_cliente_filtro_enviar_grande {
                width: 70%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar_grande:hover {
                cursor: pointer;
                opacity: .60;
            }
        }

        @media screen and (max-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: center;
            }

            #filtros {
                display: none;
            }

            #filtros_p {
                display: table;
                margin-left: 0;
                background-color: #b4b4b4;
                width: 100%;
                height: 2%;
                float: left;
            }

            #login_cliente_filtro {
                width: 100%;
            }

            #cor_filtro_pequeno {
                background: #b4b4b4;
                border: none;
            }

            #login_cliente_filtro_enviar {
                width: 80%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar:hover {
                cursor: pointer;
                opacity: .60;
            }
        }
    </style>
</head>


<!-- filtro topo grande -->
<div class="col-lg-4" id="filtros"
     style=" margin-left: 0; background-color: #b4b4b4; width: 15%; height: 100%; float: left;">
    <p style="margin-top: 15%; margin-left: 3%"><b><font size="5px">FILTRO</font> </b></p>
    <form style="margin-left: 3%">
        <p><b>Nome</b></p>
        <input type="text" class="login_cliente" style="width: 100%; height: 30px">


        <p style="margin-top: 10px"><b>CPF</b></p>
        <input type="text" class="login_cliente" style="width: 100%; height: 30px">

        <p style="margin-top: 10px"><b>CNPJ</b></p>
        <input class="login_cliente" id="login_cliente_filtro" style="width: 100%; height: 30px">


        <p style="margin-top: 10px"><b>Telefone</b></p>
        <input class="login_cliente" style="width: 100%; height: 30px">

        <p style="margin-top: 10px"><b>De:</b></p>
        <input type="date" class="login_cliente" id="login_cliente_filtro" style="width: 100%; height: 30px">

        <p style="margin-top: 10px"><b>Até:</b></p>
        <input type="date" class="login_cliente" id="login_cliente_filtro" style="width: 100%; height: 30px">
        <p>&nbsp;</p>
        <p style="margin-top: 10px"><b>Status</b></p>
        <select class="login_cliente" style="width: 100%; height: 30px">
            <option>Ativo</option>
            <option>Inativo</option>
            <option>Bloqueado</option>
        </select>
        <p>&nbsp;</p>
        <input type="submit" value="Filtrar" class="login_cliente" id="login_cliente_filtro_enviar_grande">
    </form>
</div>
<!-- começa o filtro para tela pequena e termina a grande -->
<div class="navbar-wrapper" id="filtros_p_topo">
    <nav class="navbar navbar-inverse navbar-static-top" id="cor_filtro_pequeno">
        <div class="container">
            <div class="navbar-header">
                <button type="button" id="dp" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar_filtro" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="esp_logo"></div>
                <a class="navbar-brand" href="#"></a>
                <h3>Filtros</h3>
            </div>
            <div id="navbar_filtro" class="navbar-collapse collapse">
                <div class="col-lg-4" class="navbar-toggle" id="filtros_p">
                    <form style="margin-left: 3%" action="#" method="POST">
                        <b>Nome</b>
                        <input class="login_cliente" id="login_cliente_filtro">

                        <b>CPF</b>
                        <input class="login_cliente" id="login_cliente_filtro">

                        <b>CNPJ</b>
                        <input class="login_cliente" id="login_cliente_filtro">

                        <b>Telefone</b>
                        <input type="text" class="login_cliente" id="login_cliente_filtro">

                        <b>De</b><br>

                        <input type="date" class="login_cliente" id="login_cliente_filtro">

                        <p>&nbsp;</p>
                        <input type="submit" class="login_cliente" id="login_cliente_filtro_enviar">
                    </form>

                </div>
            </div>
        </div>
    </nav>
</div>


<!-- abre caixa com o clinte -->
<div style="background-color: #e5e5e5;  height: 100px; width:80% ; margin-top: 20px;margin-left: 20%; display: table; padding-bottom: 10px">

    <div style="margin-left: 20px; margin-right: 20px; padding-top: 2%">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>CPF</th>
                <th>Telefone</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>

                    <tr>
                        <th scope="row">#5</th>
                        <td>Rafael da Silva</td>
                        <td>025.652.236-95</td>
                        <td>(11)9-9856-4563</td>
                        <td>Ativo</td>
                        <td><button  type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                            </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                        <td>



                    </tr>


            <tr>
                <th scope="row">#10</th>
                <td>Caio de Lima</td>
                <td>512.654.985-96</td>
                <td>(15)9-5469-8563</td>
                <td>Ativo</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>
            <tr>
                <th scope="row">#1</th>
                <td>Daniel Teixeira</td>
                <td>254.654.965-89</td>
                <td>(11)9-8546-6523</td>
                <td>Bloqueado</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>

            <tr>
                <th scope="row">#99</th>
                <td>Marcos Yuri</td>
                <td>546.658.963-85</td>
                <td>(11)9-6541-8954</td>
                <td>Inativo</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>

            <tr>
                <th scope="row">#17</th>
                <td>Pedro Barbosa</td>
                <td>541.846.962-96</td>
                <td>(24)9-5412-9864</td>
                <td>Bloqueado</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>

            </tr>
            <tr>
                <th scope="row">#112</th>
                <td>Alexandre Yuri</td>
                <td>541.564.523-98</td>
                <td>(12)9-5468-9874</td>
                <td>Ativo</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>

            <tr>
                <th scope="row">#78</th>
                <td>André Marcos</td>
                <td>542.658.698-98</td>
                <td>(11)9-6541-5321</td>
                <td>Inativo</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>

            <tr>
                <th scope="row">#70</th>
                <td>Rodrigo Antônio</td>
                <td>521.654.632-98</td>
                <td>(11)9-5426-9625</td>
                <td>Bloqueado</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>

            <tr>
                <th scope="row">#29</th>
                <td>André Carlos</td>
                <td>654.854.987-6</td>
                <td>(11)9-6541-9645</td>
                <td>Ativo</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>

            <tr>
                <th scope="row">#205</th>
                <td>Renata Maia</td>
                <td>412.541.652-98</td>
                <td>(11)9-6547-9456</td>
                <td>Bloqueado</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>

            <tr>
                <th scope="row">#60</th>
                <td>Maria Mercedes</td>
                <td>125.654.652-96</td>
                <td>(41)9-6541-5263</td>
                <td>Inativo</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>

            <tr>
                <th scope="row">#66</th>
                <td>Alex Ronaldo</td>
                <td>452.652.322-98</td>
                <td>(11)9-9652-9632</td>
                <td>Bloqueado</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>

            <tr>
                <th scope="row">#201</th>
                <td>Ana Maria</td>
                <td>214.548.962-85</td>
                <td>(11)9-5421-5222</td>
                <td>Inativo</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>

            <tr>
                <th scope="row">#669</th>
                <td>José Eduardo</td>
                <td>214.854.654.73</td>
                <td>(11)95485-6425</td>
                <td>Ativo</td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="location.href = 'cadastrar_usuario.php'">Visualizar
                    </button></td><td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td>


            </tr>

            </tbody>
        </table>
        <div style="margin-top: 10px; margin-bottom: 50px">
            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
        </div>

    </div>

    <!-- abrir ao clicar no cliente>
    <center>
        <div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" id="quebrar_texto_ao_centro" style="margin-top: 20px;">
                <div id="foto_perfil"><img src="imagens/clientes.png" style="width: 100%; padding: 0; margin-top: -10px;"></div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="quebrar_texto_ao_centro" style="margin-top: 20px;">
                <b>Nome do cliente</b><br>
                Ativo<br>
                Documentação<br>
                Avaliação<br>
                <font color="#8d8d8d">exemplo@email.com</font>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
              <br>  <p>Ver mais</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
                <form style="margin-top: 10px">
                    <INPUT TYPE="RADIO" NAME="OPCAO" VALUE="op1" CHECKED> Ativo
                    <INPUT TYPE="RADIO" NAME="OPCAO" VALUE="op2"> Bloqueado<br><br>
                    <button class="botao_editar" style="width: 160px"><font color="white">Editar</font></button>
                </form>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border: 1px solid #000" >
                <p>Alteração de documentação</p>
                <p>Todas as corridas</p>

                <div id="wrapper">
                    <div class="scrollbar" id="style-1">
                        <div class="force-overflow"></div>
                    </div>
                </div>

            </div>

        </div>
    </center>
    <-->
</div>


<!-- Daqui para baixo repete a div de cima -->
<!-- fecha caixa com o clinte -->