<?php echo form_open('Controller_relatorios/ajax_excel',array('id' => "form_ajax")); ?>

<input type="hidden" name="tabela" value="view_lista_grupos">
<input type="hidden" name="titulo" value="Relatório Grupos">

<!-- Filtro Campos -->
<div class="panel panel-default">

	<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
		<div class="panel-heading" role="tab" id="headingTwo">

		  <h4 class="panel-title" style="width: 100%">
		      <i class="glyphicon glyphicon-search"></i> Filtro
		  </h4>
		</div>
	</a>

	<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
	  <div class="panel-body">

	  	<?php 
	  	//Começa pela coluna 1
	  	$coluna = 1;

	  	/*Campos que devem seguir um preenchimento personalizado*/
		$coluna_nome_usuario = '<div class="col-md-3">
						<div class="form-group">
						<label class="control-label" for="usuario">Criado por:</label> 
						<select name="usuario" id="usuario" class="atualizar_campo">
						<option value="">Qualquer Um</option>';
						
						foreach ($dados_iniciais['usuarios'] as $key => $usuario) {
							$coluna_nome_usuario .= '<option value="'.$usuario->id_usuario.'">'.$usuario->nome_usuario.'</option>';
						} 

						$coluna_nome_usuario .= '</select>
						</div>
					</div>
					<input type="hidden" name="usuario_texto" id="id_usuario_texto" value="Qualquer Um">';

		$coluna_ativo = '<div class="col-md-3">
						<div class="form-group">
						<label class="control-label" for="ativo_grupo">Status:</label> 
						<select name="ativo_grupo" id="ativo_grupo" class="atualizar_campo">
						<option value="">Ambos</option>
						<option value="1">Ativo</option>
						<option value="0">Inativo</option>
						</select>
						</div>
					</div>
					<input type="hidden" name="ativo_texto" id="ativo_texto" value="Ambos">';
		/*Campos que devem seguir um preenchimento personalizado*/

	  	foreach ($dados_iniciais['campos'] as $key => $campo) {

	  		if ($campo->visivel) { //Verifica se deve ser exibido

	  			if ($coluna == 1) 
		  			echo '<div class="row">';

		  		$variavel = 'coluna_'.$campo->nome_campo;
	  			
	  			if (isset($$variavel)) { //Se existir um código para esse campo em especifico.
	  			
	  				echo $$variavel;
 
	  			} else {

	  				//Carrega o nome do campo original, caso essa seja a versão formatada
	  				$nome_campo = str_replace("_formatado", "", $campo->nome_campo);

	  				//Dependendo do tipo do campo ele recebe uma classe.
			  		switch ($campo->tipo_campo) {
			  			case 'int':
			  				$classe = "validar_numeros";
			  				break;
			  			case 'double':
			  				$classe = "validar_decimais";
			  				break;
			  			case 'float':
			  				$classe = "validar_decimais";
			  				break;
			  			case 'real':
			  				$classe = "validar_decimais";
			  				break;
			  			case 'timestamp':
			  				$classe = "range_timestamp";
			  				break;
			  			case 'date':
			  				$classe = "range_data";
			  				break;
			  			default:
			  				$classe = "";
			  				break;
			  		}


			  		echo '<div class="col-md-3">
							<div class="form-group">
								<label class="control-label" for="'.$nome_campo.'">'.$campo->descricao_campo.'</label> 
								<input class="form-control '.$classe.'" id="'.$nome_campo.'" name="'.$nome_campo.'" placeholder="'.$campo->descricao_campo.'" aviso="'.$campo->descricao_campo.'">
							</div>
						</div>';

	  			}


		  		$coluna += 1;

		  		if ($coluna == 5) {
		  			echo '</div>';
		  			$coluna = 1;
		  		}

	  		} //Verifica se existe comentário

		} 

		//Caso tenha terminado o foreach antes de fechar a ultima div.
		if ($coluna != 1) 
	  			echo '</div>';

		?>
		
		<!-- Filtro Campos -->
		<hr>
		<div class="row">

			<div class="col-md-4">
				<div class="form-group has-feedback">
					<label class="control-label" for="filtro_campo">Listar Campos:</label> 
					<select name="filtro_campo[]" id="filtro_campo" multiple="multiple">
					<?php 

					foreach ($dados_iniciais['campos'] as $key => $campo) {
						if ($campo->visivel) { //Campos que são visiveis apenas.
							if ($campo->selecionado) {
								//Pega o nome dos campos carregados no inicio, se for o mesmo deixa selecionado
								echo '<option value="'.$campo->nome_campo.'" selected>'.$campo->descricao_campo.'</option>';
							} else {
								echo '<option value="'.$campo->nome_campo.'">'.$campo->descricao_campo.'</option>';
							}
							
						}
					} ?>
					</select>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group has-feedback">
					<label class="control-label" for="filtro_ordenacao">Ordenar por:</label> 
					<select name="filtro_ordenacao[]" id="filtro_ordenacao" multiple="multiple">
						<?php foreach ($dados_iniciais['campos'] as $key => $campo) {
							if ($campo->visivel) {
								if ($campo->descricao_campo == 'ID') { //Caso seja chave primária
									echo '<option value="'.$campo->nome_campo.'" selected>'.$campo->descricao_campo.'</option>';
								} else {
									echo '<option value="'.$campo->nome_campo.'">'.$campo->descricao_campo.'</option>';
								}
								
							}
						} ?>
					</select>
				</div>
			</div>

			<div class="col-md-2">
				<div class="form-group">
					<label class="control-label" for="filtro_ordem">Ordem:</label> 
					<select name="filtro_ordem" id="filtro_ordem">
					<option value="asc">Crescente</option>
					<option value="desc">Decrescente</option>
					</select>
				</div>
			</div>

			<div class="col-md-1">
				<div class="form-group">
					<label class="control-label" for="filtro_limite">Limite de:</label> 
					<select name="filtro_limite" id="filtro_limite">
					<option value="10">10</option>
					<option value="100" selected>100</option>
					<option value="1000">1000</option>
					<option value="">Todos</option>
					</select>
				</div>
			</div>

			<div class="col-md-1" title="Filtrar">
				<a type="button" class="btn btn-info" id="filtrar" style="width: 100%; margin-top: 15px;">
					<i class="glyphicon glyphicon-search"></i>
				</a>
			</div>
			<div class="col-md-1" title="Exportar Para Excel.">
				<button type="submit" id="enviar_excel" target="_blank" class="btn btn-success" style="width: 100%; margin-top: 15px;">
					<img src="<?php echo base_url() ?>style/img/excel.png" width="20px">
				</button>
			</div>

		</div>


	  </div>
	</div>

</div>

<?php echo form_close(); ?>

<hr>
<div class="row">
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-stats"></i> Relatório Grupos</h3>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/6">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Grupo
		</a>
	</div>
</div>

<div class="progress" style="margin-top: 40px;">
  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
    <span class="sr-only">50%</span>
  </div>
</div>

<div id="load">

	<table class="table table-bordered table-hover" align="center">
		<thead align="center">
		<?php 

			foreach ($dados_iniciais['campos'] as $chave => $campo) {

				if ($campo->selecionado) {
					if ($campo->descricao_campo == "ID") 
						echo '<th style="width: 60px;" align="center" class="no-filter">Editar</th>';
					echo "<th>{$campo->descricao_campo}: </th>";
				}

			}

		?>
		</thead>
		<tbody align="center">	
			<?php
				foreach ($dados_iniciais['resultado'] as $acesso) {

					echo "<tr>";
					foreach ($dados_iniciais['campos'] as $chave => $campo) {
						if ($campo->descricao_campo == "ID") 
							echo '<td><a href="'.base_url().'main/redirecionar/4/'.$acesso[$campo->nome_campo].'"><button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></a></td>';
						if ($campo->selecionado) 
							echo "<td>".$acesso[$campo->nome_campo]."</td>";
					}
					echo "</tr>";

				}
			?>
		</tbody>
	</table>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		//Ao alterar um valor no select é atualizado o campo oculto que envia o texto selecionado para o controller, com a finalidade de exibir o texto no lugar do ID.
		$('.atualizar_campo').change(function(){

			var id = $(this).prop("id");

			$("#"+id+"_texto").val($('#'+id+' :selected').text());

		});

		$('#enviar_excel').click(function(e){
	
			e.preventDefault();
			var filtro_campo = $('#filtro_campo').val();

			if (filtro_campo.length > 0) {

				$("#form_ajax").submit();

			} else {
				$.toast({
	                  icon: 'error',
	                  position: 'top-right',
	                  hideAfter: false,
	                  heading: 'Selecione pelo menos 1 campo',
	                  text: "Campos em branco"
	              });
				return false;
			}

		});

		$(".progress").hide();

		var filtro_campo     = "";
		var filtro_ordenacao = "";
		var filtro_limite    = "";
		var filtro_ordem     = "";

		$('#filtrar').click(function(){

			filtro_campo     = $('#filtro_campo').val();
			filtro_ordenacao = $('#filtro_ordenacao').val();
			filtro_limite    = $('#filtro_limite').val();
			filtro_ordem     = $('#filtro_ordem').val();

			$('.progress').show();
			$('#load').hide();

			if (filtro_campo.length > 0) {

				$('#load').load('<?php echo base_url() ?>Controller_grupos/filtro_ajax',
								$("#form_ajax").serialize(),
				function(){

					dataTableLoad();

		            $('#load').show();
		            $('.progress').hide();

				});

			} else {
				$.toast({
                      icon: 'error',
                      position: 'top-right',
                      hideAfter: false,
                      heading: 'Selecione pelo menos 1 campo',
                      text: "Campos em branco"
                  });

				$('#load').show();
		        $('.progress').hide();
			}

		});

	});
</script>