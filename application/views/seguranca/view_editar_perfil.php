<script type="text/javascript">
	history.replaceState({pagina: "lista_usuarios"}, "Lista dos usuários ", "<?php echo base_url() ?>main/redirecionar/1");
</script>

<div class="row">
	<div class="col-md-6">
		<h1> <i class="glyphicon glyphicon-pencil"></i> Atualizar Perfil</h1>
	</div>
	<div class="col-md-6" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_historico" id="historico_modal" cod="<?php echo $this->session->flashdata('id_usuario_edicao'); ?>"> <i class="glyphicon glyphicon-time"></i> Histórico Edições</button>
	</div>
</div>
<hr>

<?php echo form_open('controller_usuarios/editar_perfil'); ?>

<!-- Campos ocultos -->
<input type="hidden" name="login_inicial" value="<?php echo $this->session->flashdata('login_usuario_edicao'); ?>">
<input type="hidden" name="email_inicial" value="<?php echo $this->session->flashdata('email_usuario_edicao'); ?>">

<div class="row">

	<div class="col-md-2">
		<div class="form-group has-feedback has-success">
			<label class="control-label" for="id_usuario">ID</label> 
			<i class="glyphicon glyphicon-lock form-control-feedback"></i>
			<input type="text" class="form-control" id="id_usuario" name="id_usuario" placeholder="ID" value="<?php echo $this->session->flashdata('id_usuario_edicao'); ?>" readonly>
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_usuario">Nome do usuário</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="nome_usuario" name="nome_usuario" placeholder="Nome usuário" aviso="Nome usuário" value="<?php echo $this->session->flashdata('nome_usuario_edicao'); ?>"  maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="email_usuario">E-mail</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_email_usuario" id="email_usuario" name="email_usuario" placeholder="E-mail" aviso="E-mail" value="<?php echo $this->session->flashdata('email_usuario_edicao'); ?>"  maxlength="40">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="telefone_usuario">Celular</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio mascara_cel" id="telefone_usuario" name="telefone_usuario" placeholder="Celular" aviso="Celular" value="<?php echo $this->session->flashdata('telefone_usuario_edicao'); ?>">
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="login_usuario">Login do usuário</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="login_usuario" name="login_usuario" placeholder="Login do usuário" aviso="Login do usuário" value="<?php echo $this->session->flashdata('login_usuario_edicao'); ?>"  maxlength="20">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="senha_usuario">Alterar Senha</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="password" class="form-control" id="senha_usuario" name="senha_usuario" placeholder="Senha" aviso="Senha">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="confirmacaoSenha">Confirme a nova Senha</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="password" class="form-control" id="confirmacaoSenha" name="confirmacaoSenha" placeholder="Confirme a Senha" aviso="Confirme a Senha">
		</div>
	</div>

</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Salvar Edição"> <i class="glyphicon glyphicon-floppy-disk"></i> Atualizar Perfil </button>
	</div>
</div>

<?php echo form_close(); ?>