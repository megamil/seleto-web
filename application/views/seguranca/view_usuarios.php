<div class="row">
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-stats"></i> Relatório Usuários</h3>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/7">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Usuário
		</a>
	</div>
</div>

<div class="progress" style="margin-top: 40px;">
  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
    <span class="sr-only">50%</span>
  </div>
</div>

<div id="load">

	<table class="table table-bordered table-hover" align="center">
		<thead align="center">
		<?php 

			foreach ($dados_iniciais['campos'] as $chave => $campo) {

				if ($campo->selecionado) {
					if ($campo->descricao_campo == "ID") 
						echo '<th style="width: 60px;" align="center" class="no-filter">Editar</th>';
					echo "<th>{$campo->descricao_campo}: </th>";
				}

			}

		?>
		</thead>
		<tbody align="center">	
			<?php
				foreach ($dados_iniciais['resultado'] as $acesso) {

					echo "<tr>";
					foreach ($dados_iniciais['campos'] as $chave => $campo) {
						if ($campo->descricao_campo == "ID") 
							echo '<td><a href="'.base_url().'main/redirecionar/5/'.$acesso[$campo->nome_campo].'"><button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></a></td>';
						if ($campo->selecionado) {
							if ($campo->descricao_campo == "Telefone") {
								echo "<td class=\"mascara_cel\">".$acesso[$campo->nome_campo]."</td>";
							} else {
								echo "<td>".$acesso[$campo->nome_campo]."</td>";
							}
						}
					}
					echo "</tr>";

				}
			?>
		</tbody>
	</table>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		//Ao alterar um valor no select é atualizado o campo oculto que envia o texto selecionado para o controller, com a finalidade de exibir o texto no lugar do ID.
		$('.atualizar_campo').change(function(){

			var id = $(this).prop("id");

			$("#"+id+"_texto").val($('#'+id+' :selected').text());

		});

		$('#enviar_excel').click(function(e){
	
			e.preventDefault();
			var filtro_campo = $('#filtro_campo').val();

			if (filtro_campo.length > 0) {

				$("#form_ajax").submit();

			} else {
				$.toast({
	                  icon: 'error',
	                  position: 'top-right',
	                  hideAfter: false,
	                  heading: 'Selecione pelo menos 1 campo',
	                  text: "Campos em branco"
	              });
				return false;
			}

		});

		$(".progress").hide();

		var filtro_campo     = "";
		var filtro_ordenacao = "";
		var filtro_limite    = "";
		var filtro_ordem     = "";

		$('#filtrar').click(function(){

			filtro_campo     = $('#filtro_campo').val();
			filtro_ordenacao = $('#filtro_ordenacao').val();
			filtro_limite    = $('#filtro_limite').val();
			filtro_ordem     = $('#filtro_ordem').val();

			$('.progress').show();
			$('#load').hide();

			if (filtro_campo.length > 0) {

				$('#load').load('<?php echo base_url() ?>Controller_usuarios/filtro_ajax',
								$("#form_ajax").serialize(),
				function(){

					dataTableLoad();

		            $('#load').show();
		            $('.progress').hide();

				});

			} else {
				$.toast({
                      icon: 'error',
                      position: 'top-right',
                      hideAfter: false,
                      heading: 'Selecione pelo menos 1 campo',
                      text: "Campos em branco"
                  });

				$('#load').show();
		        $('.progress').hide();
			}

		});

	});
</script>