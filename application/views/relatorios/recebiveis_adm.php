<?php

?>

<head>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <link href="css/menu2.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.maskedinput.js"></script>

    <style>
        @media screen and (min-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: left;
            }

            #filtros {
                display: table;
            }

            #filtros_p {
                display: none;
            }

            #filtros_p_topo {
                display: none;
            }

            #login_cliente_filtro_enviar_grande {
                width: 70%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar_grande:hover {
                cursor: pointer;
                opacity: .60;
            }
        }

        @media screen and (max-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: center;
            }

            #filtros {
                display: none;
            }

            #filtros_p {
                display: table;
                margin-left: 0;
                background-color: #b4b4b4;
                width: 100%;
                height: 2%;
                float: left;
            }

            #login_cliente_filtro {
                width: 100%;
            }

            #cor_filtro_pequeno {
                background: #b4b4b4;
                border: none;
            }

            #login_cliente_filtro_enviar {
                width: 80%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar:hover {
                cursor: pointer;
                opacity: .60;
            }
        }
    </style>
</head>


<?php
include "menu_adm.php"

?>
<body style="background-color: #ededed; padding: 0; margin: 0;">
<!-- filtro topo grande -->
<div class="col-lg-4" id="filtros"
     style=" margin-left: 0; background-color: #b4b4b4; width: 15%; height: 100%; float: left;">
    <p style="margin-top: 15%; margin-left: 3%"><b><font size="5px">FILTRO</font> </b></p>
    <p style="margin-left: 3%">
    <p><b>Nome</b></p>
    <input type="text" class="login_cliente" style="width: 100%; height: 30px">

    <p style="margin-top: 10px"><b>Fluxo de liberação</b></p>
    <input type="date" class="login_cliente" id="login_cliente_filtro" style="width: 100%; height: 30px">
    <p style="margin-top: 10px"><b>Faixa de valor De:</b></p>
    <input type="date" class="login_cliente" id="login_cliente_filtro" style="width: 100%; height: 30px">
    <p style="margin-top: 10px"><b>Ate:</b></p>
    <input type="date" class="login_cliente" id="login_cliente_filtro" style="width: 100%; height: 30px"><p style="margin-top: 10px"><b>Data De:</b></p>
    <input type="date" class="login_cliente" id="login_cliente_filtro" style="width: 100%; height: 30px">
    <p style="margin-top: 10px"><b>Ate:</b></p>
    <input type="date" class="login_cliente" id="login_cliente_filtro" style="width: 100%; height: 30px">

    <p style="margin-top: 10px"><b>Status</b></p>
    <select class="login_cliente" style="width: 100%;margin-bottom: 2%; height: 30px">
        <option>Ativo</option>
        <option>Bloqueado</option>
        <option>Liberado</option>
    </select>
    <input type="submit" class="login_cliente" id="login_cliente_filtro_enviar_grande">
    </form>
</div>
<!-- começa o filtro para tela pequena e termina a grande -->
<div class="navbar-wrapper" id="filtros_p_topo">
    <nav class="navbar navbar-inverse navbar-static-top" id="cor_filtro_pequeno">
        <div class="container">
            <div class="navbar-header">
                <button type="button" id="dp" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar_filtro" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="esp_logo"></div>
                <a class="navbar-brand" href="#"></a>
                <h3>Filtros</h3>
            </div>
            <div id="navbar_filtro" class="navbar-collapse collapse">
                <div class="col-lg-4" class="navbar-toggle" id="filtros_p">
                    <form style="margin-left: 3%" action="#" method="POST">


                        <b>Fluxo de liberação</b>
                        <input type="text" class="login_cliente" id="login_cliente_filtro">

                        <b>Data De</b>
                        <input type="date" class="login_cliente" id="login_cliente_filtro">
                        <b>Até</b>
                        <input type="date" class="login_cliente" id="login_cliente_filtro">
                        <b>Faixa de valor De:</b>
                        <input type="date" class="login_cliente" id="login_cliente_filtro">
                        <b>Até:</b>
                        <input type="date" class="login_cliente" id="login_cliente_filtro">

                        <p>&nbsp;</p>
                        <input type="submit" class="login_cliente" id="login_cliente_filtro_enviar">
                    </form>

                </div>
            </div>
        </div>
    </nav>
</div>


<!-- abre caixa com o clinte -->
<div style="background-color: #e5e5e5;  height: 100px; width:80% ; margin-top: 20px;margin-left: 20%; display: table;">
    <div style="margin-left: 20px; margin-right: 20px; padding-top: 2%">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Motorista</th>
                <th>Origem</th>
                <th>Destino</th>
                <th>Valor</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                <th scope="row">#5</th>
                <td>Caio da Silva</td>
                <td>Av Paulista, 918</td>
                <td>Rua Luiz Gama, 654</td>
                <td>R$65,00</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>


            <tr>
                <th scope="row">#10</th>
                <td>Caio da Silva</td>
                <td>Av Paulista, 2140</td>
                <td>Rua Luiz Gama, 120</td>
                <td>70,00</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>
            <tr>
                <th scope="row">#1</th>
                <td>Caio da Silva</td>
                <td>Av Paulista, 2140</td>
                <td>Rua Luiz Gama, 120</td>
                <td>60,00</td>
                <td>Em Andamento</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>

            <tr>
                <th scope="row">#99</th>
                <td>Rafael Lucas</td>
                <td>Rua Vergueiro, 2140</td>
                <td>Av Whashington Luiz, 120</td>
                <td>70,00</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>

            <tr>
                <th scope="row">#17</th>
                <td>Jean Lima </td>
                <td>Av do Estado, 215</td>
                <td>Av Paulista, 151</td>
                <td>15,99</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>

            </tr>
            <tr>
                <th scope="row">#112</th>
                <td>Rafael Lucas</td>
                <td>Rua Vergueiro, 2140</td>
                <td>Av Whashington Luiz, 120</td>
                <td>70,00</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>

            <tr>
                <th scope="row">#78</th>
                <td>Caio da Silva</td>
                <td>Av Paulista, 2140</td>
                <td>Rua Luiz Gama, 120</td>
                <td>70,00</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>

            <tr>
                <th scope="row">#112</th>
                <td>Caio da Silva</td>
                <td>Av Paulista, 918</td>
                <td>Rua Luiz Gama, 654</td>
                <td>R$65,00</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>

            <tr>
                <th scope="row">#78</th>
                <td>Jean Lima </td>
                <td>Av do Estado, 215</td>
                <td>Av Paulista, 151</td>
                <td>15,99</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>

            <tr>
                <th scope="row">#10</th>
                <td>Caio da Silva</td>
                <td>Av Paulista, 918</td>
                <td>Rua Luiz Gama, 654</td>
                <td>R$65,00</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>

            <tr>
                <th scope="row">#5</th>
                <td>Caio da Silva</td>
                <td>Av Paulista, 918</td>
                <td>Rua Luiz Gama, 654</td>
                <td>R$65,00</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>

            <tr>
                <th scope="row">#78</th>
                <td>Rafael Lucas</td>
                <td>Rua Vergueiro, 2140</td>
                <td>Av Whashington Luiz, 120</td>
                <td>70,00</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>

            <tr>
                <th scope="row">#5</th>
                <td>Caio da Silva</td>
                <td>Av Paulista, 918</td>
                <td>Rua Luiz Gama, 654</td>
                <td>R$65,00</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>

            <tr>
                <th scope="row">#78</th>
                <td>Rafael Lucas</td>
                <td>Rua Vergueiro, 2140</td>
                <td>Av Whashington Luiz, 120</td>
                <td>70,00</td>
                <td>Concluída</td>
                <td>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                    </button>
                </td>


            </tr>

            </tbody>
        </table>
    </div>
</div>

<!-- fecha caixa com o clinte -->
</div>


</body>


<?php
include "rodape.php"
?>
