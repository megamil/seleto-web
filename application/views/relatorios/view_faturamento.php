

    <style>
        @media screen and (min-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: left;
            }

            #filtros {
                display: table;
            }

            #filtros_p {
                display: none;
            }

            #filtros_p_topo {
                display: none;
            }
            #login_cliente_filtro_enviar_grande {
                width: 70%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar_grande:hover {
                cursor: pointer;
                opacity: .60;
            }
        }

        @media screen and (max-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: center;
            }

            #filtros {
                display: none;
            }

            #filtros_p {
                display: table;
                margin-left: 0;
                background-color: #b4b4b4;
                width: 100%;
                height: 2%;
                float: left;
            }

            #login_cliente_filtro {
                width: 100%;
            }

            #cor_filtro_pequeno {
                background: #b4b4b4;
                border: none;
            }

            #login_cliente_filtro_enviar {
                width: 80%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar:hover {
                cursor: pointer;
                opacity: .60;
            }
        }
        @media screen and (min-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: left;
            }

            #filtros {
                display: table;
            }

            #filtros_p {
                display: none;
            }

            #filtros_p_topo {
                display: none;
            }
            #login_cliente_filtro_enviar_grande {
                width: 70%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar_grande:hover {
                cursor: pointer;
                opacity: .60;
            }
        }

        @media screen and (max-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: center;
            }

            #filtros {
                display: none;
            }

            #filtros_p {
                display: table;
                margin-left: 0;
                background-color: #b4b4b4;
                width: 100%;
                height: 2%;
                float: left;
            }

            #login_cliente_filtro {
                width: 100%;
            }

            #cor_filtro_pequeno {
                background: #b4b4b4;
                border: none;
            }

            #login_cliente_filtro_enviar {
                width: 80%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar:hover {
                cursor: pointer;
                opacity: .60;
            }
        }
    </style>
</head>

<body>

<div id="fundo">



<div class="col-lg-4" id="filtros"
     style=" margin-left: 0; background-color: #b4b4b4; width: 15%; height: 180%; float: left;">
    <p style="margin-top: 15%; margin-left: 3%"><b><font size="5px">FILTRO</font> </b></p>
    <form style="margin-left: 3%">
        <p><b>Motorista</b></p>
        <input type="text" class="login_cliente" style="width: 100%; height: 30px">

        <p><b>De:</b></p>
        <input type="date" class="login_cliente" style="width: 100%; height: 30px">

        <p><b>Para:</b></p>
        <input type="date" class="login_cliente" style="width: 100%; height: 30px">


        <p style="margin-top: 10px"><b>Categoria</b></p>
        <select class="login_cliente" style="width: 100%; height: 30px">
            <option>PayPal</option>
            <option>Dinheiro</option>
        </select>

        <p style="margin-top: 10px"><b>Forma de pagamento</b></p>
        <select class="login_cliente" style="width: 100%; height: 30px">
            <option>Opção 1</option>
            <option>Opção 1</option>
        </select>

        <p style="margin-top: 10px"><b>Status</b></p>
        <select class="login_cliente" style="width: 100%; height: 30px">
            <option>Ativo</option>
            <option>Inativo</option>
            <option>Bloqueado</option>
        </select>
        <p>&nbsp;</p>
        <input type="submit" class="login_cliente" value="Filtrar" id="login_cliente_filtro_enviar_grande">
    </form>
</div>

<div class="col-lg-4"  id="a" style="margin-top: 20px;margin-left:3%;background:#fff;
padding: 1% 0;">
        <div class="col-lg-12 col-md-12" style="padding: 8% 1%;border: 1px solid #ccc; border-radius: 3px; margin-top: -2%">
             <p><font color="#00BD9A"><b>Faturamento total</b></font></p>
            <font size="18px"><p>R$ 10.450,56</p></font>
        </div>

    <div class="col-lg-12 col-md-12"  id="a" style="padding: 8% 1%;border: 1px solid #ccc; border-radius: 3px;margin-top: 2%">
        <div class="col-lg-12">
            <font color="#FFAC3C">  <p><b>Receitas</b></font></p>
            <div class="col-lg-6"><font color="#bbb">Receita de Empreeendedor</font><p>R$ 7958,00</p></div>
            <div class="col-lg-6"><p><font color="#Bbb"> Receita da Seleto</font></p><p>R$ 1.300,00</p></div>
        </div>
    </div>



    <div class="col-lg-12 col-md-12"  id="a" style="padding: 8% 1%;border: 1px solid #ccc; border-radius: 3px;margin-top: 2% ">
        <div class="col-lg-12">
            <p><font color="#FF4A4B"><b>Outros </b></font></p>
           <div class="col-lg-6"><font color="#bbb">Valor transação</font><p>R$ 1.300,00</p></div>
           <div class="col-lg-6"><font color="#bbb">Chargeback</font><p>R$ 1.300,00</p></div>
        </div>
    </div>

</div>


    <div class="col-lg-5"  id="a" style="margin-top: 20px;margin-left:3%;background:#fff;border: 1px solid #ccc; border-radius: 3px;
padding: 1% 0;">
        <img src="<?php echo base_url() ?>style/imagens/valor_total.png" height="600px" style="padding:0; margin: 0;margin-left: -5%;  ">
    </div>



    <!-- tabela -->



    <div style="margin-left: 20px; margin-right: 20px; padding-top: 2%; width: 80%; margin-left: 18%;">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Data</th>
                <th>Valor</th>
                <th>% Seleto</th>
                <th>% Motorista</th>
                <th>Data Prev Trans</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                <th scope="row">#5</th>
                <td>Rafael da Silva</td>
                <td>12/05/2017</td>
                <td>R$20,00</td>
                <td>R$2,50</td>
                <td>R$17,50</td>
                <td>12/05/2017</td>
                <td>Pago</td>
                <td> <a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar</button></td></a>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>



            </tr>


            <tr>
                <th scope="row">#20</th>
                <td>Lucas Silva</td>
                <td>12/05/2017</td>
                <td>R$65,00</td>
                <td>15,00</td>
                <td>R$20,00</td>
                <td>14/05/2017</td>
                <td>Bloqueado</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>
            <tr>
                <th scope="row">#1</th>
                <td>Lucas Silva</td>
                <td>12/05/2017</td>
                <td>R$65,00</td>
                <td>15,00</td>
                <td>R$20,00</td>
                <td>14/05/2017</td>
                <td>Bloqueado</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>

            <tr>
                <th scope="row">#99</th>
                <td>André Lima</td>
                <td>17/05/2017</td>
                <td>R$40,00</td>
                <td>15,00</td>
                <td>R$10,00</td>
                <td>19/05/2017</td>
                <td>À Receber</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>

            <tr>
                <th scope="row">#17</th>
                <td>Maria Teixeira</td>
                <td>12/05/2017</td>
                <td>R$80,00</td>
                <td>40,00</td>
                <td>R$20,00</td>
                <td>14/05/2017</td>
                <td>Pago</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>

            </tr>
            <tr>
                <th scope="row">#112</th>
                <td>Lucas Silva</td>
                <td>12/05/2017</td>
                <td>R$65,00</td>
                <td>15,00</td>
                <td>R$20,00</td>
                <td>14/05/2017</td>
                <td>Bloqueado</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>

            <tr>
                <th scope="row">#99</th>
                <td>André Lima</td>
                <td>17/05/2017</td>
                <td>R$40,00</td>
                <td>15,00</td>
                <td>R$10,00</td>
                <td>19/05/2017</td>
                <td>À Receber</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>

            <tr>
                <th scope="row">#112</th>
                <td>Lucas Silva</td>
                <td>12/05/2017</td>
                <td>R$65,00</td>
                <td>15,00</td>
                <td>R$20,00</td>
                <td>14/05/2017</td>
                <td>Bloqueado</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>

            <tr>
                <th scope="row">#112</th>
                <td>Lucas Silva</td>
                <td>12/05/2017</td>
                <td>R$65,00</td>
                <td>15,00</td>
                <td>R$20,00</td>
                <td>14/05/2017</td>
                <td>Bloqueado</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>

            <tr>
                <th scope="row">#112</th>
                <td>Lucas Silva</td>
                <td>12/05/2017</td>
                <td>R$65,00</td>
                <td>15,00</td>
                <td>R$20,00</td>
                <td>14/05/2017</td>
                <td>Bloqueado</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>

            <tr>
                <th scope="row">#1</th>
                <td>Lucas Silva</td>
                <td>12/05/2017</td>
                <td>R$65,00</td>
                <td>15,00</td>
                <td>R$20,00</td>
                <td>14/05/2017</td>
                <td>Bloqueado</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>

            <tr>
                <th scope="row">#99</th>
                <td>André Lima</td>
                <td>17/05/2017</td>
                <td>R$40,00</td>
                <td>15,00</td>
                <td>R$10,00</td>
                <td>19/05/2017</td>
                <td>À Receber</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>

            <tr>
                <th scope="row">#1</th>
                <td>Lucas Silva</td>
                <td>12/05/2017</td>
                <td>R$65,00</td>
                <td>15,00</td>
                <td>R$20,00</td>
                <td>14/05/2017</td>
                <td>Bloqueado</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>

            <tr>
                <th scope="row">#99</th>
                <td>André Lima</td>
                <td>17/05/2017</td>
                <td>R$40,00</td>
                <td>15,00</td>
                <td>R$10,00</td>
                <td>19/05/2017</td>
                <td>À Receber</td>
                <td><a href="corridas.php"><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Visualizar
                        </button></a></td> <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Bloquear</button></td>
                <td><button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Estornar
                    </button></td>


            </tr>

            </tbody>
        </table>
    </div>

</div>
</body>
</html>
