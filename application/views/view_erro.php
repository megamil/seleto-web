<style type="text/css">
	.panel {
		margin-top: 50px;
	}
</style>

<script type="text/javascript">
	history.replaceState({pagina: "inicio"}, "home", "<?php echo base_url() ?>");
</script>

<?php if(isset($php_error_message)){ ?>

	<div class="panel panel-danger">
		<div class="panel-heading"> 
			<h3 class="panel-title"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> Erro Interno</h3> 
		</div>
		<div class="panel-body">

			<strong>
				<a href="#" id="erro_feedback" cod="<?php echo $log_erro; ?>">Clique Aqui Para Reportar</a>
			</strong>

			<br>
			<br>

			<strong>Falha interna</strong>  <br>
			<small>Detalhes:</small> <br>
			<li>Erro: <?php $erro = explode('<br><br>',$php_error_message); echo $erro[0]; ?> </li>
			<li>Linha: <?php echo $php_error_line; ?> </li>
			<li>Função: <?php echo $php_error_filepath; ?> </li>
			<div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="headingTwo">
		      <h4 class="panel-title">
		        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
		          Ver Erro Completo.
		        </a>
		      </h4>
		    </div>
		    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
		      <div class="panel-body">
		        <div class="row">
			      	<div class="col-md-6">	
			      		<?php echo $php_error_message; ?>
			      	</div>
			      </div>
		      </div>
		    </div>
		  </div>

			<a href="<?php echo base_url(); ?>main">Voltar</a>
		</div>
	</div>


	<?php } else { ?>

<div class="panel panel-danger">
	<div class="panel-heading"> 
		<h3 class="panel-title"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> Erro 404</h3> 
	</div>
	<div class="panel-body">
		<strong>Erro 404</strong> Página não encontrada. <br>
		<a href="<?php echo base_url(); ?>main">Voltar</a>
	</div>
</div>

<?php } ?>