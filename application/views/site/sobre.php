<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo base_url() ?>style/imagens/favicon.png">

    <meta name="description" content="Seleto">
    <meta name="author" content="Naville Marketing">

    <title>Seleto</title>


    <!--TOPO -->
    <link href="<?php echo base_url() ?>style_site/css/seleto/sobre.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>style_site/css/seleto/topo.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>style_site/css/seleto/rodape.css" rel="stylesheet">




    <!-- icones -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



    <!-- icones -->


    <meta name="theme-color" content="#ff0072">


    <!-- para slide -->



    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>style_site/css/theme.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>style_site/css/theme-animate.css">

    <!-- Style Switcher-->
    <link rel="stylesheet" href="<?php echo base_url() ?>style_site/css/style-switcher.css">

    <!-- Head libs -->

    <script src="<?php echo base_url() ?>style_site/js/modernizr.js"></script>


    <script src="<?php echo base_url() ?>style_site/js/jquery.js"></script>
    <script src="<?php echo base_url() ?>style_site/js/bootstrap.js"></script>



    <script src="<?php echo base_url() ?>style_site/js/plugins.js"></script>
    <script src="<?php echo base_url() ?>style_site/js/script.js"></script>

    <!-- fim -->



    <style>
        body{font-family: 'Roboto', sans-serif;}
        #segura_sobre{
            width: 100%;
            height: 300px;
            background: url('<?php echo base_url() ?>style_site/img/bg_sobre.png') center center no-repeat;
            background-size: cover;
            display: table;
        }
        
    </style>



</head>

<body  class="front bg-pattern-dark " >


<div id="tudo">
    <div id="segura_sobre">
        <p style="margin-top: -20px">&nbsp;</p>

    </div>
        <div id="segura_sobre_p2" >
            <div class="col-lg-4 tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                <img src="<?php echo base_url() ?>style_site/img/motorista-1.png" width="100%">
            </div>
            <div class="col-lg-5">
                <h3 class="tm30 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.6s"" ><a style="border-bottom: 8px solid #fdb14e;padding-bottom: 8px;">A Seleto</a></h3>
                <p style="font-size: 13px;text-align: justify;color:#131313" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                    A Seleto é uma empresa Mineira de aplicativos para celulares Android e iPhone. Conectamos diversos serviços de Empreendedores Individuais e empresas à Clientes/usuários. Temos orgulho de Minas Gerais e trabalhamos pensando nos mineiros. Se você compartilha do nosso sentimento, venha conhecer a Seleto e descubra tudo que ela tem a te oferecer.

                </p>
                <div class="col-lg-2 tm30 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s" id="botoes_app" style="display:table;">
                    <img src="<?php echo base_url() ?>style_site/img/app-store.png" height="40px">
                </div>
                <div class="col-lg-2 tm30 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
                    <img src="<?php echo base_url() ?>style_site/img/app-store.png" height="40px">
                </div>
            </div>
            <div class="col-lg-12">
                <p style="margin-top: 20px">&nbsp;</p>
            </div>
            <div class="col-lg-4 tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                <img src="<?php echo base_url() ?>style_site/img/motorista-2.png" width="100%">
            </div>
            <div class="col-lg-5">
                <h3 class="tm30 wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.6s"><a style="border-bottom: 8px solid #3f61ad;padding-bottom: 8px;" >Objetivo</a></h3>
                <p style="font-size: 13px;text-align: justify;color: #131313" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">

                    Contribuir com a mobilidade urbana, otimizando e modernizando a forma como as pessoas se locomovem de um ponto a outro, promover a inclusão social, criar oportunidades para empreendedores individuais contribuindo para o desenvolvimento humano dos cidadãos, fomentar a geração de novos negócios aumentando a renda per capta familiar e regional, contribuir movimentando o comercio nos seguimentos associados aos serviços disponibilizados e oferecer não apenas um App de tecnologia, mas uma ferramenta segura, moderna e prática de utilidade ao público da cidade de Barbacena e região do Campo das Vertentes.
                </p>
            </div>
        </div>

    <div id="parte2" style="margin-top: 35px" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s"">

        <div class="col-lg-6" style="background: #e2e2e2;height: 350px;background: url('<?php echo base_url() ?>style_site/img/bg_azul.png')">

            <h2 id="titulo_esquerdo">  <img src="<?php echo base_url() ?>style_site/img/certo.png"  height="40px"> PRINCÍPIOS</h2>
            <p id="parte2_texto_esquerdo" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s" >
                Ética e transparência. Qualidade
                e excelência. Confiança e Respeito.
                Integridade e honestidade.</p>
            </p>
        </div>
        <div class="col-lg-6" style="background: #3f61ad;height: 350px;background: url('<?php echo base_url() ?>style_site/img/bg_laranja.png')">

           <h2 id="titulo_direito" class="col-lg-12">  <img src="<?php echo base_url() ?>style_site/img/certo.png"  height="40px"> ÁREA DE ATUAÇÃO</h2>
            <p id="parte2_texto_direito"  class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                Barbacena e Região de Campos
                das Vertentes. Um aplicativo
                Mineiro feito para Mineiros.</p>
        </div>
    </div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<div id="segura3">
    <h2>  <img src="<?php echo base_url() ?>style_site/img/certo.png"  height="40px"> LEGALIDADE DESSE SIRVIÇO</h2>
    <div id="linha_laranja" class="tm30 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.6s"></div>
<p style="color:#000;">
    “As atividades da Plataforma Seleto e dos Empreendedores Individuais que a contratam são garantidas pela Constituição Federal Brasileira (“CRFB”), pela Lei Federal nº 12.587/2012, que instituiu a Política Nacional de Mobilidade Urbana, pelo Código Civil (“CC/02”) e pela Lei Federal nº 12.965/14, conhecida como o Marco Civil da Internet no Brasil. A CRFB protege as liberdades de iniciativa (art. 1º, IV, e art. 170), de concorrência (art. 170, IV) e de exercício de qualquer trabalho (art. 5º, XIII). Tais liberdades garantem que o transporte individual no Brasil não pode ser objeto de monopólio, podendo ser exercido por todos aqueles que desejem se lançar a tal atividade, inclusive por meio da Plataforma Seleto. Tais garantias constitucionais também fundamentam a atividade da Seleto. A Seleto é uma empresa de intermediação tecnológica que desenvolveu um aplicativo para conectar provedores de serviços a usuários interessados nos serviços disponibilizados por Empreendedores e presentes no App Seleto. Tal atividade atende ao que dispõe o Marco Civil da Internet no Brasil, que garante, em seu art. 3º, VIII, a liberdade dos modelos de negócio na internet. Ressaltamos que a plataforma Seleto está aberta a colaborar com qualquer tipo de regulação no que tange mobilidade urbana. A legislação federal e as liberdades constitucionais garantem que os Empreendedores contratantes da Plataforma Seleto possam atuar livremente e decisões do Poder Judiciário de Minas Gerais tem amparado a legalidade de tais atividades.
</p>
</div>


</div>


        </div>

<p>&nbsp;</p>
<p>&nbsp;</p>



</body>
</html>