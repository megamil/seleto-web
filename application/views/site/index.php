<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="Seleto">
        <meta name="author" content="Naville Marketing">
        <link rel="shortcut icon" href="<?php echo base_url() ?>style/imagens/favicon.png">

        <title>Seleto</title>


        <!--TOPO -->
        <link href="<?php echo base_url() ?>style_site/css/seleto/home.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>style_site/css/seleto/topo.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>style_site/css/seleto/rodape.css" rel="stylesheet">




        <!-- icones -->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



        <!-- icones -->


        <meta name="theme-color" content="#232d5a">


        <!-- para slide -->
        <link rel="stylesheet" href="<?php echo base_url() ?>style_site/css/theme-animate.css">



        <!-- Theme CSS -->

        <!-- Style Switcher-->
        <link rel="stylesheet" href="<?php echo base_url() ?>style_site/css/style-switcher.css">

        <!-- Head libs -->

        <script src="<?php echo base_url() ?>style_site/js/modernizr.js"></script>


        <script src="<?php echo base_url() ?>style_site/js/jquery.js"></script>
        <script src="<?php echo base_url() ?>style_site/js/bootstrap.js"></script>



        <script src="<?php echo base_url() ?>style_site/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>style_site/js/script.js"></script>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>style_site/js/jquery.waterwheelCarousel.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var carousel = $("#carousel").waterwheelCarousel({
                    flankingItems: 3,
                    movingToCenter: function ($item) {
                        $('#callback-output').prepend('movingToCenter: ' + $item.attr('id') + '<br/>');
                    },
                    movedToCenter: function ($item) {
                        $('#callback-output').prepend('movedToCenter: ' + $item.attr('id') + '<br/>');
                    },
                    movingFromCenter: function ($item) {
                        $('#callback-output').prepend('movingFromCenter: ' + $item.attr('id') + '<br/>');
                    },
                    movedFromCenter: function ($item) {
                        $('#callback-output').prepend('movedFromCenter: ' + $item.attr('id') + '<br/>');
                    },
                    clickedCenter: function ($item) {
                        $('#callback-output').prepend('clickedCenter: ' + $item.attr('id') + '<br/>');
                    }
                });

                $('#prev').bind('click', function () {
                    carousel.prev();
                    return false
                });

                $('#next').bind('click', function () {
                    carousel.next();
                    return false;
                });

                $('#reload').bind('click', function () {
                    newOptions = eval("(" + $('#newoptions').val() + ")");
                    carousel.reload(newOptions);
                    return false;
                });

            });
        </script>
    </head>
    <body>

    <!--PArte 1-->
    <div id="parte1">
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" id="bg_parte_1" style="background: url('<?php echo base_url() ?>style_site/img/banner.png') center center  no-repeat;background-size: cover">
            <p class="tm30 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1.8s">

            <h1 id="titulo1" class="tm30 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.6s">Aproximando</h1>
            <h1 id="titulo2" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">o seu destino</h1>
            <div id="linha_laranja" class="tm30 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.6s"></div>

            <!-- Botoes app -->



            </p><!-- fecha primeiro p lado esquerdo -->
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  id="bg_parte_1_d">
            <p id="titulo_box">Viaje com a SELETO</p>
            <div id="segura_cinza">
                <div id="dentro_box_cinza" >
                    <p class="titulo_cinza" style="color: #fff;font-weight: bolder;margin-top: 5px">Cadastre-se como passageiro ou Motorista
                        e utilize os serviços da Seleto.</p>
                    <div id="segura_botoes" class="center-block">
                        <button type="button" class="btn btn-success col-lg-5 col-md-5 col-sm-5 col-xs-5 center-block" id="botao_motorista">Motorista</button>
                        <button type="button" class="btn btn-success col-lg-5 col-md-5 col-sm-5 col-xs-5" id="botao_passagero">Passageiro</button>
                    </div>

                    <form action="#" method="POST">
                        <input type="text" class="cadastrar" placeholder="Nome Completo">
                        <input type="text" class="cadastrar" placeholder="Telefone">
                        <input type="text" class="cadastrar" placeholder="E-mail">
                        <input type="password" class="cadastrar" placeholder="Senha">
                        <input type="password" class="cadastrar" placeholder="confirmar Senha">
                        <button type="button" class="btn btn-success col-lg-5" id="botao_cadastrar">Cadastrar</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>




<!-- PARTE 1.5 -->


    <div id="parte1_5" >
        <div class="col-lg-12 col-md-12 col-sm-12" style="background: url('<?php echo base_url() ?>style_site/img/banner_amarelo.png') center center no-repeat;
                background-size: cover;padding: 20px 0">
            <p style="text-align: center;color: #fff;font-size: 25px" class="tm30 wow fadeInDown" data-wow-duration="2s" data-wow-delay="0.6s">Baixe já seu novo aplicativo de viagens</p>
            <div class="col-lg-12 col-md-12 col-sm-12 center-block tm30 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s" id="botoes_app" style="display:table;">
              <center><img src="<?php echo base_url() ?>style_site/img/app-store.png" height="55px" id="botao_aple">
                <img src="<?php echo base_url() ?>style_site/img/google-play.png" height="55px"></center>
            </div>
        </div>
    </div>




    <!--PArte 2-->



    <div id="parte2">

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="background: #e2e2e2;height: 350px">
            <p id="parte2_texto_esquerdo" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s" >
                A maioria das categorias dos Empreendedores oferece transporte de passageiros com tarifas diferenciadas, para diferentes tipos de público, e se diferenciam em função da economia e do padrão. Além das categorias que fazem transporte privado de passageiros, os Empreendedores disponibilizam serviços de utilidade que fazem parte do dia a dia das pessoas.
            </p>
            </p>
        </div>
        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12" style="background: #3f61ad;height: 350px">
            <p id="parte2_texto_direito"  class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
                A Seleto é uma Plataforma de Tecnologia que se distingue de outras nos tipos de serviço que oferece aos Usuários e Empreendedores. Nela existem várias modalidades de serviço disponíveis, onde você escolhe qual quer usar ou a que te atende melhor em determinada ocasião.
            </p>
        </div>
    </div>



    <!--PArte 3-->
    <div id="parte3" style="background: url('<?php echo base_url() ?>style_site/img/bg_3.png') center center  no-repeat;background-size: cover">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center">

            <!-- note: should work with jQuery 1.4 and up -->


            <style type="text/css">


            </style>



            <div id="carousel" >
                    <img src="<?php echo base_url() ?>style_site/img/taxi_bloco.png"  alt="Image 1" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s" />
                    <img src="<?php echo base_url() ?>style_site/img/taxi_bloco.png" alt="Image 2" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s" />
                    <img src="<?php echo base_url() ?>style_site/img/taxi_bloco.png"  alt="Image 3" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s"/>
                    <img src="<?php echo base_url() ?>style_site/img/taxi_bloco.png" alt="Image 4" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.9s" />
                    <img src="<?php echo base_url() ?>style_site/img/taxi_bloco.png"  alt="Image 5" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.2s" />
            </div>
            <p style="margin-top: -250px">&nbsp;</p>
               <span href="#" id="prev" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">
                   <i class="fa fa-1x fa-angle-left" aria-hidden="true"></i>
               </span>
                <span id="espaco_next_prev">&nbsp;</span>
                <span href="#" id="next" class="tm30 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="2.2s">
                    <i class="fa fa-1x fa-angle-right" aria-hidden="true"></i>
                </span>
                <a id="saiba"  class="tm30 wow fadeIn" data-wow-duration="2s" data-wow-delay="2.5s">Saiba mais</a>


        </div>
    </div>



    <!--PArte 4-->
    <div id="parte4" style="background: url('<?php echo base_url() ?>style_site/img/bg_3.png') center center  no-repeat;background-size: cover;text-align: center">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <p  class="titulos_4 tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.2s">FÁCIL DE SOLICITAR</p>
            <img src="<?php echo base_url() ?>style_site/img/celular.png"  class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.2s" />
            <p class="texto_parte4 tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.2s" >
                O App reconhece sua localização e o Empreendedor te encontra onde você estiver. Depois de encerrada a prestação do serviço, você poderá, se quiser, nos enviar comentários sobre o que achou da experiência! Ao final de cada serviço.
            </p>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <p  class="titulos_4 tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">PRONTO QUANDO VOCÊ PRECISAR</p>
            <img src="<?php echo base_url() ?>style_site/img/carro.png"  class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s" />
            <p  class="texto_parte4 tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">
                Não importa para onde você está indo, sempre haverá um Seleto pronto para te atender. O App Seleto é 100% Nativo, foi desenvolvido para os mineiros, iniciando as atividades em Barbacena e na região do Campo das Vertentes.
            </p>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="margin-bottom: 10px">
            <p  class="titulos_4 tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.9s">OPÇÕES PARA QUALQUER OCASIÃO</p>
            <img src="<?php echo base_url() ?>style_site/img/porquinho.png"  class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.9s" />
            <p  class="texto_parte4 tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.9s">
                Na Plataforma de Tecnologia Seleto existem várias opções de serviços disponibilizados por Empreendedores Individuais. Escolha o que melhor que lhe servir ou experimente todas! Pois para cada ocasião existe um Seleto ideal para você.
            </p>

        </div>
    </div>




    <div id="parte5">
        a
    </div>


    <!--PArte 5.5-->
    <div id="parte5_5" style="background: url('<?php echo base_url() ?>style_site/img/bg_5_5.png') center center  no-repeat;background-size: cover;text-align: center">
        <p  class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.9s" style="font-size: 30px;font-weight: bolder;margin-top: 10px;color: #000">PARCEIROS</p>
       <div id="parceiros">
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding: 20px">
                <img src="<?php echo base_url() ?>style_site/img/camada-7.png"  class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s" style="width: 100%" />
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"  style="padding: 20px">
               <img src="<?php echo base_url() ?>style_site/img/camada-4.png"  class="parceiro tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s"  style="width: 100%"/>
             </div>

           <p class="hidden-lg hidden-sm hidden-md"> &nbsp;</p>

           <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding: 20px">
               <img src="<?php echo base_url() ?>style_site/img/camada-5.png"  class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s" style="width: 100%" />
           </div>
           <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"  style="padding: 20px">
               <img src="<?php echo base_url() ?>style_site/img/camada-6.png"  class="parceiro tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".6s"  style="width: 100%"/>
           </div>

       </div>
    </div>

    </body>
</html>