
<nav class="navbar navbar-inverse" id="topo">
    <div class="container-fluid" id="alinhar_topo">
        <div class="navbar-header">
            <button style="border: none" type="button" id="topo_pequeno" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand tm30 wow   fadeInDown" data-wow-duration="1s" data-wow-delay="0.6s" href="index.php" ><img src="<?php echo base_url() ?>style_site/img/logo.png" style="width: 120px;margin-top: -3px"></a>
        </div>
        <div class="collapse navbar-collapse"  id="myNavbar">
            <ul class="nav navbar-nav" id="alinhar">
                <li class="alinhar_links" ><a href="<?php echo base_url() ?>controller_site/sobre" style="color: #fff"> &nbsp;Sobre</a></li>
                <li class="alinhar_links"><a href="#" style="color: #fff"> &nbsp;Usuário</a></li>
                <li class="alinhar_links"><a href="#" style="color: #fff">Empreendedor</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right" >
                <li class="alinhar_links tm30 wow   fadeInDown" data-wow-duration="1s" data-wow-delay="0.6s"><a href="#" style="color: #fff">Ajuda</a></li>
                <li class="alinhar_links tm30 wow   fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s"><a href="#" style="color: #fff">SAC</a></li>
                <li class="alinhar_links tm30 wow   fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s"><a href="#" style="color: #fff;background-color: #03951b;padding-left: 30px;padding-right: 30px;">Entrar</a></li>
                </ul>
        </div>
    </div>
</nav>
