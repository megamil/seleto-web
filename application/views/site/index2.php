<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Seleto">
    <meta name="author" content="Naville Marketing">
    <link rel="shortcut icon" href="<?php echo base_url() ?>style/imagens/favicon.png">

    <title>Seleto</title>


    <!--TOPO -->
    <link href="<?php echo base_url() ?>style_site/css/seleto/home.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>style_site/css/seleto/topo.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>style_site/css/seleto/rodape.css" rel="stylesheet">




    <!-- icones -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



    <!-- icones -->


    <meta name="theme-color" content="#ff0072">


    <!-- para slide -->



    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>style_site/css/theme.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>style_site/css/theme-animate.css">

    <!-- Style Switcher-->
    <link rel="stylesheet" href="<?php echo base_url() ?>style_site/css/style-switcher.css">

    <!-- Head libs -->

    <script src="<?php echo base_url() ?>style_site/js/modernizr.js"></script>


    <script src="<?php echo base_url() ?>style_site/js/jquery.js"></script>
    <script src="<?php echo base_url() ?>style_site/js/bootstrap.js"></script>



    <script src="<?php echo base_url() ?>style_site/js/plugins.js"></script>
    <script src="<?php echo base_url() ?>style_site/js/script.js"></script>

    <!-- fim -->

    <style type="text/css">
        #banner{
            width: 100%;
            display: table;
            color: #fff;
                position: absolute;
            background: url('<?php echo base_url() ?>style_site/img/banner.png') center center no-repeat;


        }
        @media screen and (min-width: 1461px){
           #banner{
               background-size: 80%;
               background-position-x: -30%;
           }
        }
        @media screen and (max-width: 1460px){
            #banner{
                background-size: 100%;
            }
        }
    </style>


    <style>
    body{font-family: 'Roboto', sans-serif;}
</style>



</head>

<body  class="front bg-pattern-dark " >


<div id="tudo" style="background: #ccc">
    <div id="banner" >
        <div id="segura_como_f_bg" >




                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" id="textos">
                    <p class="tm30 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1.8s">

                        <h1 id="titulo1" class="tm30 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.6s">Aproximando</h1>
                        <h1 id="titulo2" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">o seu destino</h1>
                        <div id="linha_laranja" class="tm30 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.6s"></div>

                    <!-- Botoes app -->



                    </p><!-- fecha primeiro p lado esquerdo -->
                 </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s" id="box_cinza">
                        <p id="titulo_box">Viaje com a SELETO</p>
                        <div id="dentro_box_cinza" >
                           <p class="titulo_cinza">Cadastre-se como passageiro ou Motorista
                             e utilize os serviços da Seleto.</p>
                            <div id="segura_botoes" class="center-block">
                                <button type="button" class="btn btn-success col-lg-5 center-block" id="botao_motorista">Motorista</button>
                                <button type="button" class="btn btn-success col-lg-5" id="botao_passagero">Passageiro</button>
                            </div>

                            <form action="#" method="POST">
                                <input type="text" class="cadastrar" placeholder="Nome Completo">
                                <input type="text" class="cadastrar" placeholder="Telefone">
                                <input type="text" class="cadastrar" placeholder="E-mail">
                                <input type="password" class="cadastrar" placeholder="Senha">
                                <input type="password" class="cadastrar" placeholder="confirmar Senha">
                                <button type="button" class="btn btn-success col-lg-5" id="botao_cadastrar">Cadastrar</button>
                            </form>
                        </div>
                    </div>

                 </div>
       </div>

    </div>
</div>

<div id="parte1.5" >
    <div class="col-lg-12 col-md-12 col-sm-12" style="background: url('<?php echo base_url() ?>style_site/img/banner_amarelo.png') center center no-repeat;
            background-size: cover;padding: 20px 0">
        <p style="text-align: center;color: #fff;font-size: 25px">Baixe já seu novo aplicativo de viagens</p>
        <div class="col-lg-4 col-md-4 col-sm-4"></div>
        <div class="col-lg-2 col-md-2 col-sm-2 tm30 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s" id="botoes_app" style="display:table;">
            <img src="<?php echo base_url() ?>style_site/img/app-store.png" height="55px" style="float: right">
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 tm30 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
            <img src="<?php echo base_url() ?>style_site/img/google-play.png" height="55px">
        </div>
    </div>
</div>

<div id="parte2">

    <div class="col-lg-6 col-md-6 col-sm-6" style="background: #e2e2e2;height: 350px">
        <p id="parte2_texto_esquerdo" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s" >
            A maioria das categorias dos Empreendedores oferece transporte de passageiros com tarifas diferenciadas, para diferentes tipos de público, e se diferenciam em função da economia e do padrão. Além das categorias que fazem transporte privado de passageiros, os Empreendedores disponibilizam serviços de utilidade que fazem parte do dia a dia das pessoas.
            </p>
        </p>
    </div>
    <div class="col-lg-6  col-md-6 col-sm-6" style="background: #3f61ad;height: 350px">
        <p id="parte2_texto_direito"  class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">
            A Seleto é uma Plataforma de Tecnologia que se distingue de outras nos tipos de serviço que oferece aos Usuários e Empreendedores. Nela existem várias modalidades de serviço disponíveis, onde você escolhe qual quer usar ou a que te atende melhor em determinada ocasião.
        </p>
    </div>
</div>


<!-- parete 3 -->

<div id="parte3">
    <div id="segura_parte3">
<p>&nbsp;</p>
        <div id="bloco_3_tudo">
            <div id="meio_3">
                a
            </div>
        </div>


    </div>
</div>


<!-- parte 4 -->

<div id="parte4">
    <div id="segura_parte4" >
        <div class="texto_parte4 col-lg-4  col-md-4 col-sm-4" style="height: 400px;background: url('<?php echo base_url() ?>style_site/img/camada-1.png') center center no-repeat;
                background-size: cover;" >
            <p>&nbsp;</p>
            <p style="color:#fdb14e;font-weight: bolder;margin-bottom: 40px" class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">FÁCIL DE SOLICITAR</p>
            <img src="<?php echo base_url() ?>style_site/img/celular.png" style="width: 50px;margin-top: -3px"  class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">
            <p>&nbsp;</p>
            <p style="color:#fdb14e;"  class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">
            O App reconhece sua localização e o Empreendedor te encontra onde você estiver. Depois de encerrada a prestação do serviço, você poderá, se quiser, nos enviar comentários sobre o que achou da experiência! Ao final de cada serviço.
            </p>
        </div>
        <div class="texto_parte4 col-lg-4  col-md-4 col-sm-4" style="height: 400px;background: url('<?php echo base_url() ?>style_site/img/camada-2.png') center center no-repeat;
                background-size: cover;"  >
            <p>&nbsp;</p>
            <p style="color:#fdb14e;font-weight: bolder;margin-bottom: 40px"  class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.0s">PRONTO QUANDO VOCÊ PRECISAR</p>
            <img src="<?php echo base_url() ?>style_site/img/carro.png" style="width: 100px;margin-top: -3px"  class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.0s">
            <p>&nbsp;</p>
            <p style="color:#fdb14e;"  class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.0s">
            Não importa para onde você está indo, sempre haverá um Seleto pronto para te atender. O App Seleto é 100% Nativo, foi desenvolvido para os mineiros, iniciando as atividades em Barbacena e na região do Campo das Vertentes.
            </p>
        </div>
        <div class="texto_parte4 col-lg-4 col-md-4 col-sm-4" style="height: 400px;background: url('<?php echo base_url() ?>style_site/img/camada-3.png') center center no-repeat;
                background-size: cover;" >
            <p>&nbsp;</p>
            <p style="color:#fdb14e;font-weight: bolder;margin-bottom: 40px"  class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.12s">OPÇÕES PARA QUALQUER OCASIÃO</p>
            <img src="<?php echo base_url() ?>style_site/img/porquinho.png" style="width: 90px;margin-top: -3px"  class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.12s">
            <p>&nbsp;</p>
            <p style="color:#fdb14e;"  class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.12s">
            Na Plataforma de Tecnologia Seleto existem várias opções de serviços disponibilizados por Empreendedores Individuais. Escolha o que melhor que lhe servir ou experimente todas! Pois para cada ocasião existe um Seleto ideal para você.
            </p>
        </div>

    </div>
</div>






<div id="parte4.5" style="height: 600px;text-align: center">
    <p>&nbsp;</p>

<h2 style="text-align: center;font-weight: bolder;">PARCEIROS</h2>
    <div class="col-lg-3 col-md-3 col-sm-3">
        <img src="<?php echo base_url() ?>style_site/img/camada-7.png" id="parceiros"  class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.12s">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3">
        <img src="<?php echo base_url() ?>style_site/img/camada-4.png" id="parceiros" class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.0s">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3" >
        <img src="<?php echo base_url() ?>style_site/img/camada-5.png" id="parceiros"  class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3">
        <img src="<?php echo base_url() ?>style_site/img/camada-6.png" id="parceiros"   class=" tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="2.0s">
    </div>
    <p>&nbsp;</p>
</div>



<!-- parte 5 -->

<div id="parte5">
    <div id="segura_parte5" class="center-block">
        <p>&nbsp;</p>
        <h1 id="titulo_parte5" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">Área de Atuação</h1>
        <p id="texto_parte5" class="tm30 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">Barbacena e Região de Campos das Vertentes.
            Um aplicativo Mineiro feito para Mineiros.</p>

    </div>
</div>


</body>
</html>