<?php

?>

<head>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <link href="css/menu2.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.maskedinput.js"></script>

    <style>
        @media screen and (min-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: left;
            }

            #filtros {
                display: table;
            }

            #filtros_p {
                display: none;
            }

            #filtros_p_topo {
                display: none;
            }

            #login_cliente_filtro_enviar_grande {
                width: 70%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar_grande:hover {
                cursor: pointer;
                opacity: .60;
            }
        }

        @media screen and (max-width: 768px) {
            #quebrar_texto_ao_centro {
                text-align: center;
            }

            #filtros {
                display: none;
            }

            #filtros_p {
                display: table;
                margin-left: 0;
                background-color: #b4b4b4;
                width: 100%;
                height: 2%;
                float: left;
            }

            #login_cliente_filtro {
                width: 100%;
            }

            #cor_filtro_pequeno {
                background: #b4b4b4;
                border: none;
            }

            #login_cliente_filtro_enviar {
                width: 80%;
                background: #ededed;
                margin-left: 10%;
                margin-top 70%;
                border: none;
            }

            #login_cliente_filtro_enviar:hover {
                cursor: pointer;
                opacity: .60;
            }
        }
    </style>
</head>


<?php
include "menu_adm.php"

?>
<body style="background-color: #ededed; padding: 0; margin: 0;">
<!-- abre caixa com o clinte -->
<div style="background-color: #e5e5e5;  height: 100px; width:30% ; margin-top: 40px;margin-left: 35%; display: table;">
    <div style="margin-left: 20px; margin-right: 20px; padding-top: 2%">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Status</th>
                <th>Data</th>
                <th>Assunto</th>
            </tr>
            </thead>
            <tbody>

            <tr>
                <th scope="row">Ativo</th>
                <td>05/08/2017</td>
                <td>Aviso</td>
                   <td> <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Responder
                       </button>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Excluir
                    </button>
                </td>


            </tr>


            <tr>
                <th scope="row">Ativo</th>
                <td>05/08/2017</td>
                <td>Aviso</td>
                <td> <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Responder
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Excluir
                    </button>
                </td>


            </tr>
            <tr>
                <th scope="row">Ativo</th>
                <td>05/08/2017</td>
                <td>Aviso</td>
                <td> <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Responder
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Excluir
                    </button>
                </td>

            </tr>

            <tr>
                <th scope="row">Ativo</th>
                <td>05/08/2017</td>
                <td>Aviso</td>
                <td> <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Responder
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Excluir
                    </button>
                </td>

            </tr>

            <tr>
                <th scope="row">Ativo</th>
                <td>05/08/2017</td>
                <td>Aviso</td>
                <td> <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Responder
                    </button>
                    <button type="submit" class="btn btn-success formulario-banner" onclick="ShowDiv8()">Excluir
                    </button>
                </td>

            </tr>

            </tbody>
        </table>
    </div>
</div>

<!-- fecha caixa com o clinte -->
</div>


</body>


<?php
include "rodape.php"
?>
