<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_webservice extends CI_Model {

		private $code = null;
		private $message = null;
		private $query = null;
		private $funcao = null;

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();

			    $erro = array(
			    				//'fk_usuario' => $this->session->userdata('usuario'),
								'cod' => $this->code,
								'erro' => $this->message,
								'query' => $this->query,
								'funcao' => $this->funcao,
								'maquina_usuario_erro' => $_SERVER['HTTP_USER_AGENT']
			    			);
			    
			    //Gerando arquivo de erro.
			    log_message('error', 
			    			'Codigo: '.$this->code.' Mensagem: "'.$this->message.'" Query: "'.$this->query.'"');
			    
			    // //Armazenando no banco o log.
			     $this->db->insert('seg_log_erro',$erro);

			    return array('status' => false, 
			    			 'log_erro' => $this->db->insert_id(),
			    			 'code' => $this->code, 
			    			 'message' => $this->message, 
			    			 'query' => $this->query);

			} else {
			    $this->db->trans_commit();
			    return array('status' => true);
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}

		############################### Querys ###############################

		private $usuario;
		private $senha;

		public function validar_login(){

			$this->db->select('id_usuario,nome_usuario,email_usuario,senha_usuario,fk_grupo_usuario,ativo_usuario');
			$this->db->from('seg_usuarios');
			$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
			$this->db->where('email_usuario',$this->get_('email_usuario'));
			$this->db->where('senha_usuario',$this->get_('senha_usuario'));
			$this->db->where('fk_grupo_usuario >=',6); //Motorista
			//$this->db->where('fk_grupo_usuario <=',7); // Cliente
			$login = $this->db->get()->row();

			// echo $this->db->last_query();
			// die();

			if (isset($login) && $login->ativo_usuario) {
				return $login;
			} else {
				return false;
			}

		}

		public function validar_login_facebook($id_facebook) {

			$this->db->select('id_usuario,nome_usuario,email_usuario,senha_usuario,fk_grupo_usuario,ativo_usuario');
			$this->db->from('seg_usuarios');
			$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
			$this->db->where('id_facebook',$id_facebook);
			$this->db->where('fk_grupo_usuario >=',6); //Motorista
			$this->db->where('fk_grupo_usuario <=',7); // Cliente
			$login = $this->db->get()->row();

			// echo $this->db->last_query();
			// die();

			if (isset($login) && $login->ativo_usuario) {
				return $login;
			} else {
				return false;
			}

		}


		public function set_($campo,$valor){
			$this->$campo = $valor;
		}

		public function get_($campo){
			return $this->$campo;
		}

		//Pré cadastro
		public function preCadastroCliente(){

			return array (

				'sexo' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 1')->result(),
				'pagamento' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 2')->result(),
				'operadoras' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 3')->result(),
				'estados' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 4')->result()

			);

		}

		public function novoCliente($valores = null){

			$usuario = array (
				'ativo_usuario' 	=> 1,
				'nome_usuario' 		=> $valores['nome_usuario'],
				'email_usuario' 	=> $valores['email_usuario'],
				'login_usuario' 	=> $valores['email_usuario'],
				'telefone_usuario'  => $valores['telefone_usuario'],
				'senha_usuario' 	=> $valores['senha_usuario'],
				'id_facebook' 		=> $valores['id_facebook'],
				'fk_grupo_usuario' 	=> $valores['fk_grupo_usuario']
			);

			$this->db->insert('seg_usuarios',$usuario);
			
			$e = $this->db->error();
			if ($e['code'] != 0) { //Erro no Primeiro insert

				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_webservice / novoCliente';
				return false;		

			} else {

				$cliente = array (
					'fk_usuario'          => $this->db->insert_id(),
					'sexo_cliente'        => $valores['sexo_cliente'],
					'cpf_cliente'      	  => $valores['cpf_cliente'],
					'identidade_cliente'  => $valores['identidade_cliente'],
					'pagamento_cliente'   => $valores['pagamento_cliente'],
					'rua_cliente'		  => $valores['rua_cliente'],
					'numero_end_cliente'  => $valores['numero_end_cliente'],
					'complemento_cliente' => $valores['complemento_cliente'],
					'bairro_cliente'      => $valores['bairro_cliente'],
					'cep_cliente' 		  => $valores['cep_cliente'],
					'cidade_cliente' 	  => $valores['cidade_cliente'],
					'estado_cliente' 	  => $valores['estado_cliente'],
					'operadora_cliente'   => $valores['operadora_cliente']
				);

				$this->db->insert('cad_cliente',$cliente);

				$e = $this->db->error();
				if ($e['code'] != 0) { // Erro no segundo insert
					$this->code = $e['code'];
					$this->message = $e['message'];	
					$this->query = $this->db->last_query();
					$this->funcao = 'Model_webservice / novoCliente';
					return false;		
				} else {
					return $this->db->insert_id();
				}

			}

		}

		public function preEdicaoCliente($id_usuario){

			return array (

				'sexo' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 1')->result(),
				'pagamento' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 2')->result(),
				'operadoras' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 3')->result(),
				'estados' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 4')->result(),

				'selecionados' => $this->db->query("select id_usuario,
															nome_usuario,
															email_usuario,
															telefone_usuario,
															id_facebook,
															sexo_cliente,
															fk_grupo_usuario,
															cpf_cliente,
															identidade_cliente,
															pagamento_cliente,
															rua_cliente,
															numero_end_cliente,
															complemento_cliente,
															bairro_cliente,
															cep_cliente,
															cidade_cliente,
															estado_cliente,
															operadora_cliente

																from cad_cliente 
																inner join seg_usuarios on id_usuario = fk_usuario
															where fk_usuario = {$id_usuario}")->row()

			);

		}

		public function editarCliente($valores = null){

			$usuario = array (
				'ativo_usuario' 	=> 1,
				'nome_usuario' 		=> $valores['nome_usuario'],
				'email_usuario' 	=> $valores['email_usuario'],
				'login_usuario' 	=> $valores['email_usuario'],
				'telefone_usuario'  => $valores['telefone_usuario'],
				'senha_usuario' 	=> $valores['senha_usuario'],
				'id_facebook' 		=> $valores['id_facebook'],
				'fk_grupo_usuario' 	=> $valores['fk_grupo_usuario']
			);

			$this->db->where('id_usuario',$valores['id_usuario']);
			$this->db->update('seg_usuarios',$usuario);
			
			$e = $this->db->error();
			if ($e['code'] != 0) { //Erro no Primeiro update

				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_webservice / editarCliente';
				return false;		

			} else {

				$cliente = array (
					'fk_usuario'          => $valores['id_usuario'],
					'sexo_cliente'        => $valores['sexo_cliente'],
					'cpf_cliente'      	  => $valores['cpf_cliente'],
					'identidade_cliente'  => $valores['identidade_cliente'],
					'pagamento_cliente'   => $valores['pagamento_cliente'],
					'rua_cliente'		  => $valores['rua_cliente'],
					'numero_end_cliente'  => $valores['numero_end_cliente'],
					'complemento_cliente' => $valores['complemento_cliente'],
					'bairro_cliente'      => $valores['bairro_cliente'],
					'cep_cliente' 		  => $valores['cep_cliente'],
					'cidade_cliente' 	  => $valores['cidade_cliente'],
					'estado_cliente' 	  => $valores['estado_cliente'],
					'operadora_cliente'   => $valores['operadora_cliente']
				);

				$this->db->where('fk_usuario',$valores['id_usuario']);
				$this->db->update('cad_cliente',$cliente);

				$e = $this->db->error();
				if ($e['code'] != 0) { // Erro no segundo update
					$this->code = $e['code'];
					$this->message = $e['message'];	
					$this->query = $this->db->last_query();
					$this->funcao = 'Model_webservice / editarCliente';
					return false;		
				} else {
					return $this->db->insert_id();
				}

			}

		}

		//Pré Cadastro Motorista
		public function preCadastroMotorista(){

			return array (

				'sexo' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 1')->result(),
				'pagamento' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 2')->result(),
				'operadoras' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 3')->result(),
				'estados' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 4')->result(),
				'categorias' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 5')->result()
				//'carros' => $this->db->query('select id_tipo_carro as id, tipo_carro nome from cad_tipo_carro')->result()

			);

		}

		public function novoMotorista($valores = null) {

			$usuario = array (
				'ativo_usuario' 	=> 1,
				'nome_usuario' 		=> $valores['nome_usuario'],
				'email_usuario' 	=> $valores['email_usuario'],
				'login_usuario' 	=> $valores['email_usuario'],
				'telefone_usuario'  => $valores['telefone_usuario'],
				'senha_usuario' 	=> $valores['senha_usuario'],
				'id_facebook' 		=> $valores['id_facebook'],
				'fk_grupo_usuario' 	=> $valores['fk_grupo_usuario']
			);

			$this->db->insert('seg_usuarios',$usuario);
			
			$e = $this->db->error();
			if ($e['code'] != 0) { //Erro no Primeiro insert

				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_webservice / novoCliente';
				return false;		

			} else {

				$motorista = array (
					'fk_usuario'          => $this->db->insert_id(),
					'sexo_motorista'        => $valores['sexo_motorista'],
					'cpf_motorista'      	  => $valores['cpf_motorista'],
					'identidade_motorista'  => $valores['identidade_motorista'],
					'pagamento_motorista'   => $valores['pagamento_motorista'],
					'rua_motorista'		  => $valores['rua_motorista'],
					'numero_end_motorista'  => $valores['numero_end_motorista'],
					'complemento_motorista' => $valores['complemento_motorista'],
					'bairro_motorista'      => $valores['bairro_motorista'],
					'cep_motorista' 		  => $valores['cep_motorista'],
					'cidade_motorista' 	  => $valores['cidade_motorista'],
					'estado_motorista' 	  => $valores['estado_motorista'],
					'operadora_motorista'   => $valores['operadora_motorista'],

					//'fk_carro'      	  => $valores['fk_carro'],
					'cnh_num_motorista'   => $valores['cnh_num_motorista'],
					'cnh_cat_motorista'   => $valores['cnh_cat_motorista'],
					'cnh_validade'   	  => $valores['cnh_validade']
				);

				$this->db->insert('cad_motorista',$motorista);

				$e = $this->db->error();
				if ($e['code'] != 0) { // Erro no segundo insert
					$this->code = $e['code'];
					$this->message = $e['message'];	
					$this->query = $this->db->last_query();
					$this->funcao = 'Model_webservice / novoMotorista';
					return false;		
				} else {
					return $this->db->insert_id();
				}

			}

		}

		public function preEdicaoMotorista($id_usuario){

			return array (

				'sexo' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 1')->result(),
				'pagamento' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 2')->result(),
				'operadoras' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 3')->result(),
				'estados' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 4')->result(),
				'categorias' => $this->db->query('select id_item_grupo as id , nome_item_grupo as nome from cad_item_grupo where fk_grupo = 5')->result(),
				//'carros' => $this->db->query('select id_tipo_carro as id, tipo_carro nome from cad_tipo_carro')->result(),

				'selecionados' => $this->db->query("select id_usuario,
															nome_usuario,
															email_usuario,
															telefone_usuario,
															id_facebook,
															sexo_motorista,
															fk_grupo_usuario,
															cpf_motorista,
															identidade_motorista,
															pagamento_motorista,
															rua_motorista,
															numero_end_motorista,
															complemento_motorista,
															bairro_motorista,
															cep_motorista,
															cidade_motorista,
															estado_motorista,
															operadora_motorista,
															cnh_num_motorista,
															cnh_cat_motorista,
															cnh_validade

																from cad_motorista 
																inner join seg_usuarios on id_usuario = fk_usuario
															where fk_usuario = {$id_usuario}")->row()

			);

		}


		public function editarMotorista($valores) {

			$usuario = array (
				'ativo_usuario' 	=> 1,
				'nome_usuario' 		=> $valores['nome_usuario'],
				'email_usuario' 	=> $valores['email_usuario'],
				'login_usuario' 	=> $valores['email_usuario'],
				'telefone_usuario'  => $valores['telefone_usuario'],
				'senha_usuario' 	=> $valores['senha_usuario'],
				'id_facebook' 		=> $valores['id_facebook'],
				'fk_grupo_usuario' 	=> $valores['fk_grupo_usuario']
			);

			$this->db->where('id_usuario',$valores['id_usuario']);
			$this->db->update('seg_usuarios',$usuario);
			
			$e = $this->db->error();
			if ($e['code'] != 0) { //Erro no Primeiro update

				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_webservice / editarMotorista';
				return false;		

			} else {

				$motorista = array (

					'sexo_motorista'         => $valores['sexo_motorista'], 
					'cpf_motorista'          => $valores['cpf_motorista'],
					'identidade_motorista'   => $valores['identidade_motorista'],
					'pagamento_motorista'    => $valores['pagamento_motorista'],
					'rua_motorista'          => $valores['rua_motorista'],
					'numero_end_motorista'   => $valores['numero_end_motorista'],
					'complemento_motorista'  => $valores['complemento_motorista'],
					'bairro_motorista'       => $valores['bairro_motorista'],
					'cep_motorista'          => $valores['cep_motorista'],
					'cidade_motorista'       => $valores['cidade_motorista'],
					'estado_motorista'       => $valores['estado_motorista'],
					'operadora_motorista'    => $valores['operadora_motorista'],
					//'fk_carro'      		 => $valores['fk_carro'],
					'cnh_num_motorista'      => $valores['cnh_num_motorista'],
					'cnh_cat_motorista'      => $valores['cnh_cat_motorista'],
					'cnh_validade'           => $valores['cnh_validade']

				);

				$this->db->where('fk_usuario',$valores['id_usuario']);
				$this->db->update('cad_motorista',$motorista);

				$e = $this->db->error();
				if ($e['code'] != 0) { // Erro no segundo update
					$this->code = $e['code'];
					$this->message = $e['message'];	
					$this->query = $this->db->last_query();
					$this->funcao = 'Model_webservice / editarMotorista';
					return false;		
				} else {
					return $this->db->insert_id();
				}

			}

		}

		public function notificaDeslocamento($valores) {
			$this->db->where('id_motorista', $valores['id_motorista']);
			$this->db->update('cad_motorista', $valores);

			$e = $this->db->error();
			if ($e['code'] != 0) { //Erro no update

				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_webservice / notificaDeslocamento';
				return false;		

			} else {
				return true;
			}
		}

		/*Lista dos carros WS11*/
		public function preCorrida($id) {
			
			$sexo = $this->db->query("select count(*) total ,sexo_cliente from cad_cliente where fk_usuario = {$id['id_usuario']} and fk_status = 48")->row()->sexo_cliente;
			
			if(!is_null($sexo) && $sexo == 2){
				return $this->db->query("select id_tipo_carro as id, tipo_carro as titulo, descricao_carro as descricao, tarifa_carro as valor_km
									 from cad_tipo_carro where ativo_carro = 1")->result();
			} else {
				return $this->db->query("select id_tipo_carro as id, tipo_carro as titulo, descricao_carro as descricao, tarifa_carro as valor_km
									 from cad_tipo_carro where ativo_carro = 1 and pink_carro = 0")->result();
			}

			
		}

	}










