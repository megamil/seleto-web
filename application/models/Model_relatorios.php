<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_relatorios extends CI_Model {

		private $code = null;
		private $message = null;
		private $query = null;
		private $funcao = null;

		function __construct() {
		    parent::__construct();
		}

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();

			    $erro = array(
			    				'fk_usuario' => $this->session->userdata('usuario'),
								'cod' => $this->code,
								'erro' => $this->message,
								'query' => $this->query,
								'funcao' => $this->funcao,
								'maquina_usuario_erro' => $_SERVER['HTTP_USER_AGENT']
			    			);
			    
			    //Gerando arquivo de erro.
			    log_message('error', 
			    			'Codigo: '.$this->code.' Mensagem: "'.$this->message.'" Query: "'.$this->query.'"');
			    
			    //Armazenando no banco o log.
			    $this->db->insert('seg_log_erro',$erro);

			    return array('status' => false, 
			    			 'log_erro' => $this->db->insert_id(),
			    			 'code' => $this->code, 
			    			 'message' => $this->message, 
			    			 'query' => $this->query);

			} else {
			    $this->db->trans_commit();
			    return array('status' => true);
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}
		############################### QUERYS ###############################

		/*Filtro Ajax*/
		public function listarCampos($tabela = null,$selecionados = null){

			$campos_select = "";
			foreach ($selecionados as $posicao => $valor) {
				$campos_select .= "'{$valor}'";
				if ($posicao < count($selecionados)-1)  
					$campos_select .= ",";
			}

			return $this->db->query("select *, (nome_campo in ({$campos_select})) as selecionado
												from cad_detalhes_views 
												where nome_view = '{$tabela}'")->result_array();

		}

		public function filtroAjax($parametros = null,$filtro = null){

			$this->db->select($parametros['filtro_campo'])
					 ->from($parametros['tabela']);

			foreach ($filtro as $key => $campo) {

				//echo 'Tipo: '.$campo['tipo'].'<br> Coluna: '.$campo['nome_campo'].'<br> Valor: '.$campo['valor'].'<br>';

				$coluna_filtrar = $campo['nome_campo'];

				switch ($campo['tipo_campo']) {
		  			case 'double':
		  				$this->db->where($coluna_filtrar,str_replace(',', '.', $campo['valor']));
		  				break;
		  			case 'float':
		  				$this->db->where($coluna_filtrar,str_replace(',', '.', $campo['valor']));
		  				break;
		  			case 'real':
		  				$this->db->where($coluna_filtrar,str_replace(',', '.', $campo['valor']));
		  				break;
		  			case 'int':
		  				$this->db->where($coluna_filtrar,$campo['valor']);
		  				break;

		  			case 'timestamp':

		  				//Mantem o nome do campo original para buscar sem a formação como timestamp

		  				$periodo = explode(' até ', $campo['valor']);
		  				$this->db->where($campo['nome_campo']." >= ",$this->data(str_replace('/','-',$periodo[0]),true));
		  				$this->db->where($campo['nome_campo']." <= ",$this->data(str_replace('/','-',$periodo[1]),true));
		  				break;

		  			case 'date':

		  				//Mantem o nome do campo original para buscar sem a formação como data

		  				$periodo = explode(' até ', $campo['valor']);
		  				$this->db->where($campo['nome_campo']." >= ",$this->data(str_replace('/','-',$periodo[0]),false));
						$this->db->where($campo['nome_campo']." <= ",$this->data(str_replace('/','-',$periodo[1]),false));
		  				break;

		  			default:
		  				$this->db->like("lower(".$coluna_filtrar.")",strtolower($campo['valor']));
		  				break;
		  		}	
			}

			if (isset($parametros['filtro_ordenacao'])) {
				foreach ($parametros['filtro_ordenacao'] as $por) {
					if (in_array($por, $parametros['filtro_campo'])) {
						$this->db->order_by($por,$parametros['filtro_ordem']);
					}
				}
			}
			
			$this->db->limit($parametros['filtro_limite']);

			return $this->db->get()->result_array();

			//Para testar 
			// $sql = $this->db->get_compiled_select();
			// echo $sql;

		}
		/*Filtro Ajax*/

		###################################################################################################
		
		/*Acessos*/
		public function view_relatorio_acesso(){

			/*[ALTERAR CONFORME PRECISE]*/
			$campos_select = "'usuario','data_acesso_formatado','ip','maquina','acessou'";
			//As aspas simples não são aceitas como campo, mas para lógica de informar quais campos estão sendo passados "Váriavel campos no array" é preciso.

			$this->db->select(str_replace("'", "", $campos_select));
			$this->db->limit(100);

			/*[ALTERAR CONFORME PRECISE]*/
			return array(

						 'resultado' => 
							$this->db->get("view_relatorio_acessos")->result_array(),
						 'usuarios' => 
						 	$this->db->query('SELECT id_usuario, nome_usuario from seg_usuarios')->result(),
						 'campos' => /*Carrega as informações para montar a lista automaticamente 
						 				(Caso não tenha uma view listar os campos manualmente)*/
						 	$this->db->query("select *, (nome_campo in ({$campos_select})) as selecionado
												from cad_detalhes_views 
												where nome_view = 'view_relatorio_acessos' and visivel = true;")->result()
						);

		}

		public function view_relatorio_navegacao(){

			/*[ALTERAR CONFORME PRECISE]*/
			$campos_select = "'nome_usuario','data_log_formatado','com_permissao','descricao_aplicacao'";
			//As aspas simples não são aceitas como campo, mas para lógica de informar quais campos estão sendo passados "Váriavel campos no array" é preciso.

			$nome_view = "view_relatorio_navegacao";

			$this->db->select(str_replace("'", "", $campos_select));
			$this->db->limit(100);

			/*[ALTERAR CONFORME PRECISE]*/
			return array(

						 'resultado' => 
							$this->db->get($nome_view)->result_array(),
						 'usuarios' => 
						 	$this->db->query('SELECT id_usuario, nome_usuario from seg_usuarios')->result(),
						 'aplicacoes' => 
						 	$this->db->query('SELECT id_aplicacao, descricao_aplicacao from seg_aplicacao')->result(),
						 'campos' => /*Carrega as informações para montar a lista automaticamente 
						 				(Caso não tenha uma view listar os campos manualmente)*/
						 	$this->db->query("select *, (nome_campo in ({$campos_select})) as selecionado
												from cad_detalhes_views 
												where nome_view = '{$nome_view}' and visivel = true;")->result()
						);

		}

		public function view_relatorio_edicoes(){

			/*[ALTERAR CONFORME PRECISE]*/
			$campos_select = "'nome_usuario','data_log_formatado','campo','original_edicao','novo_edicao','descricao_aplicacao'";
			//As aspas simples não são aceitas como campo, mas para lógica de informar quais campos estão sendo passados "Váriavel campos no array" é preciso.

			$nome_view = "view_relatorio_edicoes";

			$this->db->select(str_replace("'", "", $campos_select));
			$this->db->limit(100);

			/*[ALTERAR CONFORME PRECISE]*/
			return array(

						 'resultado' => 
							$this->db->get($nome_view)->result_array(),
						 'usuarios' => 
						 	$this->db->query('SELECT id_usuario, nome_usuario from seg_usuarios')->result(),
						 'aplicacoes' => 
						 	$this->db->query('SELECT id_aplicacao, descricao_aplicacao from seg_aplicacao')->result(),
						 'campos' => /*Carrega as informações para montar a lista automaticamente 
						 				(Caso não tenha uma view listar os campos manualmente)*/
						 	$this->db->query("select *, (nome_campo in ({$campos_select})) as selecionado
												from cad_detalhes_views 
												where nome_view = '{$nome_view}' and visivel = true;")->result()
						);

		}

		public function loadHistoricoEdicoes($id_aplicacao = null){
			return $this->db->query("select nome_usuario,data_log_formatado,campo,original_edicao,novo_edicao
										from
										view_relatorio_edicoes
										where fk_aplicacao = {$this->session->userdata("id_aplicacao_atual")} and id_edicao = {$id_aplicacao}")->result();
		}

		/*Acessos FIM*/
		public function data($data = null){

			if ($data != "") {

				return date("Y-m-d H:i:s",strtotime(str_replace('/','-',$data)));
				
			} else {
				return 0;
			}

		}

	}