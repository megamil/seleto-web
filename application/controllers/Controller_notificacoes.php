<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_notificacoes extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_notificacoes');
		    
	}


	public function criar_notificacao(){

		$this->form_validation->set_rules('titulo_notificacao','Título','required');
		$this->form_validation->set_rules('notificacao','Descrição','required');

		$dados = array (

			'fk_usuario'              => $this->session->userdata('usuario'),
			'titulo_notificacao'      => $this->input->post('titulo_notificacao'),
			'notificacao'             => $this->input->post('notificacao'),
			'data_limite_notificacao' => $this->data($this->input->post('data_limite_notificacao'))

		);

		if ($this->form_validation->run()) {

			$this->model_notificacoes->start();
			$id = $this->model_notificacoes->create($dados);

			$this->model_notificacoes->notificar($id,$this->input->post('fk_usuario_destino'),$this->input->post('id_grupo'),$this->input->post('fk_usuario_nao_destino'));

			$commit = $this->model_notificacoes->commit();
			
			if ($commit['status']) {
				$this->aviso('Notificação enviada','Notificação enviada com sucesso!','success',false);

				redirect('main/redirecionar/11/'.$id);
			} else {

				$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

				$this->session->set_flashdata($dados);
				redirect('main/redirecionar/10');
			}

		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);

			$this->session->set_flashdata($dados);
			redirect('main/redirecionar/10');

		}

	}

	public function editar_notificacao(){

		$this->form_validation->set_rules('titulo_notificacao','Título','required');
		$this->form_validation->set_rules('notificacao','Descrição','required');

		$dados = array (

			'id_notificacao'          => $this->input->post('id_notificacao'),
			'fk_usuario'              => $this->session->userdata('usuario'),
			'titulo_notificacao'      => $this->input->post('titulo_notificacao'),
			'notificacao'             => $this->input->post('notificacao'),
			'data_limite_notificacao' => $this->data($this->input->post('data_limite_notificacao'))

		);

		if ($this->form_validation->run()) {

			$this->model_notificacoes->start();
			$this->model_notificacoes->update($dados);

			$commit = $this->model_notificacoes->commit();
			
			if ($commit['status']) {
				$this->aviso('Notificação editada','Notificação editada com sucesso!','success',false);

				redirect('main/redirecionar/12/'.$this->input->post('id_notificacao'));
			} else {

				$this->aviso('Falha ao editar','Erro(s) ao atualizar dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

				$this->session->set_flashdata($dados);
				redirect('main/redirecionar/11/'.$this->input->post('id_notificacao'));
			}

		} else {

			$this->aviso('Falha ao editar','Erro(s) no formulário: '.validation_errors(),'error',true);

			$this->session->set_flashdata($dados);
			redirect('main/redirecionar/11/'.$this->input->post('id_notificacao'));

		}

	}

	public function load_notificacao(){

		$usuario = $this->session->userdata('usuario');
		$id = $this->input->post('id');

		$this->model_notificacoes->start();
		$notificacao = $this->model_notificacoes->loadNotificacao($usuario,$id);

		$commit = $this->model_notificacoes->commit();
			
		if ($commit['status']) {

			echo "<strong>Título: </strong> {$notificacao->titulo_notificacao} <br>
				  <strong>Descrição: </strong> <br> 
				  	{$notificacao->notificacao}
				  <br>
				  <hr>";

			echo "<small>Data Publicação: {$notificacao->data_envio_notificacao}</small> / ";
			
			if ($notificacao->data_limite_notificacao != 0)
				echo "<small>Disponível até: {$notificacao->data_limite_notificacao}</small>  / ";
			
			if ($notificacao->data_leitura != 0)
				echo "<small>Lido em: {$notificacao->data_leitura}</small> ";

		} else {
			echo 'Erro ao Carregar: '.$commit['message'].'';
		}


	}

	public function ajax_Notificacoes(){
		
		header('Content-Type: application/json; charset=utf-8');

		$this->model_notificacoes->start();
		
		$dados = $this->model_notificacoes->ajaxNotificacoes();

		$commit = $this->model_notificacoes->commit();
			
		if ($commit['status'])
			echo json_encode($dados);	

	}

	public function upload_imagem(){

		$uploaddir = $_SERVER['DOCUMENT_ROOT'].'/'.base_url().'upload/notificacoes/';

		//Gera um nome único para o arquivo
		$arquivo = basename(time().uniqid(md5($_FILES['imagem']['name']))).'.png';
		$uploadfile = $uploaddir.$arquivo;
		$editor = $_POST['editor'];

		echo '<pre>';
		if (move_uploaded_file($_FILES['imagem']['tmp_name'], $uploadfile)) {
		    echo "Arquivo válido.\n";
		} else {
		    echo "Arquivo inválido!\n";
		}

		echo 'DEBUG:';
		print_r($_FILES);

		echo '</pre>';

		$image_type = array('image/jpeg', 'image/jpg', 'image/gif', 'image/png', 'image/x-png', 'image/bmp');

		echo '<script>';
		if(in_array($_FILES['imagem']['type'], $image_type)) {
			echo 'window.parent.'.$editor.'.insertContent("<img src=\''.base_url().'upload/notificacoes\/'.$arquivo.'\' />");';
		} else {
			echo 'window.parent.'.$editor.'.insertContent("Erro: Arquivo num formato inválido");';
			unlink($uploadfile);
		}
		echo 'window.parent.'.$editor.'.plugins.upload.finish();';
		echo '</script>';
		
	}	

	public function aviso($titulo,$aviso,$tipo,$fixo){

		//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
							$aviso_ = str_replace('
				', '', $aviso);

		$aviso = str_replace('\'', '"', $aviso_);

		$this->session->set_flashdata('titulo_alerta',$titulo);
		$this->session->set_flashdata('mensagem_alerta',$aviso);
		$this->session->set_flashdata('tipo_alerta',$tipo);
		$this->session->set_flashdata('mensagem_fixa',$fixo);

	}

	public function data($data = null){
		
		if ($data != "" && $data != null) {
			$data_ = explode('/',$data);
			return $data_[2].'-'.$data_[1].'-'.$data_[0];
		} else {
			return 0;
		}

	}

	######################################################################################################
	###### Notificações PUSH

	public function notificando() {

            $tokens = $this->model_notificacoes->carregarFirebaseTokens();
            
            $devicesToken = array ();

            $resultadoHTML = "";

            $prontos = 0; //Garante que passou por todos token
            $titulo = strtoupper($this->input->get("titulo"));
            $mensagem = strtoupper($this->input->get("mensagem"));

            while ($prontos < $tokens->num_rows()) {

                $devicesToken[] = $tokens->row($prontos)->token;
                //echo $tokens->row($prontos)->token;
                    
                $prontos += 1;
                if(count($devicesToken) == 999) { //Garante que não passará o limite de envio
                    $gcpm = new FCMPushMessage();
                    $gcpm->setDevices($devicesToken);

                    $response = $gcpm->send(array('mensagem' => $mensagem,'titulo' => $titulo));
                    //Atualizar token ou remover, caso necessário.
                    $resultadoHTML .= $this->atualizarBD($response, $devicesToken);

                    $resultadoHTML .= "<br><hr><br>";

                    $devicesToken = array(); //zera o array, e fica pronto para mais 999 tokens

                }

            }
            
            if(count($devicesToken) > 0) { //Garante que se não entrar no if do while, enviara os tokens existentes no array.
                $gcpm = new FCMPushMessage();
                $gcpm->setDevices($devicesToken);

                $response = $gcpm->send(array('mensagem' => $mensagem,'titulo' => $titulo));

                $resultadoHTML .= $this->atualizarBD($response, $devicesToken);

                $resultadoHTML .= "<br><hr><br>";

            }
            //Conferir envio das notificações.
            //echo $resultadoHTML;

            $array = array ("status" => "1", "resultado" => "Nova publicação enviada com sucesso! notificado a {$prontos} aparelhos. {$titulo} / {$mensagem}");
            echo json_encode ( $array );  
      

    }

    public function atualizarBD($response = null, $devicesToken = null) {
        // RESULT JSON
                $html = '';
                $resultJson = json_decode($response);
                foreach($resultJson as $key=>$value){
                    if(is_array($value)){
                        $html .= $key.'=>{<br/>';
                        $i = 0;
                        
                        foreach($value as $k=>$v){
                            $html .= '&nbsp;&nbsp;&nbsp;&nbsp;{&nbsp;';
                            foreach($v as $kObj=>$vObj){
                                $html .= $kObj.'=>'.$vObj;
                                
                                // UPDATE REG ID
                                    if(strcasecmp($kObj, 'registration_id') == 0 && strlen( trim($vObj) ) > 0){

                                        $novo = trim($vObj);

                                        if($this->model_notificacoes->atualizarToken($novo,$devicesToken[$i])) {
                                        $html .= " Token : {$devicesToken[$i]} Atualizado com sucesso para: {$novo}";
                                        } else {
                                            $html .= " Falha na atualização.";
                                        }

                                    }
                                // DELETE REG ID
                                    else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'NotRegistered') == 0){

                                        if($this->model_notificacoes->deletarToken($devicesToken[$i])) {
                                            $html .= " Token: {$devicesToken[$i]} Deletado com sucesso. NotRegistered";
                                        } else {
                                            $html .= " Token: {$devicesToken[$i]} Falhou ao deletar. NotRegistered";
                                        }

                                    }
                                    else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'MismatchSenderId') == 0){

                                        if($this->model_notificacoes->deletarToken($devicesToken[$i])) {
                                            $html .= " Token: {$devicesToken[$i]} Deletado com sucesso. MismatchSenderId";
                                        } else {
                                            $html .= " Token: {$devicesToken[$i]} Falhou ao deletar. MismatchSenderId";
                                        }

                                    } else if(strcasecmp($kObj, 'error') == 0 && strcasecmp($vObj, 'InvalidRegistration') == 0){
                                        
                                        if($this->model_notificacoes->deletarToken($devicesToken[$i])) {
                                            $html .= " Token: {$devicesToken[$i]} Deletado com sucesso. InvalidRegistration";
                                        } else {
                                            $html .= " Token: {$devicesToken[$i]} Falhou ao deletar. InvalidRegistration";
                                        }

                                    } else {

                                        $html .= " Token {$devicesToken[$i]} OK";

                                    }
                                    
                                $html .= '<br />';
                            }
                            $html = rtrim($html, '<br />');
                            $html .= '&nbsp;}<br />';
                            $i++;
                        }
                        $html .= '}<br />';
                    }
                    else{
                        $html .= $key.'=>'.$value.'<br />';
                    }
                }

                return $html; // PRINT RESULT

    }

    public function adicionar_Token(){

    	$token['token'] = $this->input->get('token');
    	$token['fk_usuario'] = $this->input->get('fk_usuario');
    	$token['aparelho'] = $this->input->get('aparelho');

    	if(!$this->model_notificacoes->novo_Token($token)){

    		$array = array ("status" => "0", "resultado" => "Falha");
			echo json_encode ( $array );  

    	} else {

    		$array = array ("status" => "1", "resultado" => "Sucesso");
			echo json_encode ( $array );  

    	}


    }

} // FIM Controller_notificacoes

class FCMPushMessage {

    var $url = 'https://fcm.googleapis.com/fcm/send';
    //Chave do Firebase
    var $serverApiKey = "DEFINIR CHAVE";
    var $devices = array();
    

    function setDevices($deviceIds){
    
        if(is_array($deviceIds)){
            $this->devices = $deviceIds;
        } else {
            $this->devices = array($deviceIds);
        }
    
    }

    function send($data = null){
        
        if(!is_array($this->devices) || count($this->devices) == 0){
            $this->error("Nenhum Aparelho informado");
        }
        
        if(strlen($this->serverApiKey) < 8){
            $this->error("API Key não informada");
        }

        $notificacao = array
        (
            "body"      => $data['mensagem'],
            "title"     => $data['titulo'],
            "badge"     => 1,
            'vibrate'   => 1,
            'sound'     => 1
        );
        
        $campos = array(
            'registration_ids'  => $this->devices,
            'content_available' => true,
            'notification'      => $notificacao,
            'priority'          => 'high',
            'data'              => $data
        );
        

        $headers = array( 
            'Authorization: key=' . $this->serverApiKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        
        curl_setopt( $ch, CURLOPT_URL, $this->url );
        
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $campos ) );
        
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $result = curl_exec($ch);
        
        curl_close($ch);
        
        return $result;
    }
    
    function error($msg){
        echo "Falha ao enviar:";
        echo "\t" . $msg;
        exit(1);
    }

}