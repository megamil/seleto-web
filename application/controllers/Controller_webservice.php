<?php defined('BASEPATH') OR exit('No direct script access allowed');

	require APPPATH . '/libraries/REST_Controller.php';
	use Restserver\Libraries\REST_Controller;

	class Controller_webservice extends REST_Controller  {

		function __construct() {
			// Construct the parent class
			parent::__construct();
			// Configure limits on our controller methods
			// Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
			$this->methods['perfil_get']['limit'] = 500; // 500 requests per hour per user/key
			$this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
			$this->methods['perfileditar_put']['limit'] = 50; // 50 requests per hour per user/key
			$this->load->model('model_webservice');
			$this->load->model('model_usuarios');
		}

		public function autenticacao(){ //Garante que quem está usando o sistema possúi credenciais de acesso.

			if (!isset($_SERVER['PHP_AUTH_USER']) 
				|| empty($_SERVER['PHP_AUTH_USER'])
				|| !isset($_SERVER['PHP_AUTH_PW']) 
				|| empty($_SERVER['PHP_AUTH_PW'])) { //Usuário e / ou senha em Branco
			    
				return false;

			} else {

			    $this->model_webservice->set_('email_usuario',$_SERVER['PHP_AUTH_USER']);
				$this->model_webservice->set_('senha_usuario',($_SERVER['PHP_AUTH_PW']));
				if ($this->model_webservice->validar_login()) {
					
					return true;

				} else {

					return false;

				}

			}

		}

		//Login
		public function perfil_login_get() {

			header('Content-Type: text/html; charset=utf-8');

			if ($this->autenticacao()) {
				
				$valores = array('email_usuario' => $this->get('email_usuario'), 'senha_usuario' => $this->get('senha_usuario'));

				$this->form_validation->set_data($valores);
				$this->form_validation->set_rules('email_usuario','E-mail usuário','required');
				$this->form_validation->set_rules('senha_usuario','Senha','required');

				if ($this->form_validation->run()) {

					$this->model_webservice->set_('email_usuario',$valores['email_usuario']);
					$this->model_webservice->set_('senha_usuario',($valores['senha_usuario']));
					$dados = $this->model_webservice->validar_login();

					if ($dados) {

						$retornar = array(

							'id_usuario' => $dados->id_usuario,
							'nome_usuario' => $dados->nome_usuario,
							'email_usuario' => $dados->email_usuario,
							'senha_usuario' => $dados->senha_usuario,
							'grupo_usuario' => $dados->fk_grupo_usuario

						);
						
						//DEFINIR REGRAS DE GRUPOS **************************************
						if ($dados->fk_grupo_usuario != 6 && $dados->fk_grupo_usuario != 7) {
							
							$this->response(array('status' => 0, 'resultado' => 'Usuário sem permissão de acesso.'),Self::HTTP_UNAUTHORIZED); //401

						} else if($dados->ativo_usuario == 0) {

							$this->response(array('status' => 0, 'resultado' => 'Usuário inativo.'),Self::HTTP_UNAUTHORIZED); //401

						} else {

							if ($dados->fk_grupo_usuario == 6) {
								$this->response(array('status' => 1, 'resultado' => 'Login Motorista efetuado com sucesso','usuario' => $retornar),Self::HTTP_OK); //200
							} else if ($dados->fk_grupo_usuario == 7) {
								$this->response(array('status' => 1, 'resultado' => 'Login Cliente efetuado com sucesso','usuario' => $retornar),Self::HTTP_OK); //200
							} else {

							}

						}

					} else {
						$this->response(array('status' => 0, 'resultado' => 'Dados não localizados!'),Self::HTTP_NOT_FOUND); //404
					}

				} else { //Campos Preenchidos

					$erros = strip_tags(validation_errors());
					$this->response(array('status' => 0, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400

				}

			} else {

				$id_facebook = $this->get('id_facebook');

				if (isset($id_facebook)) {
					
					$dados = $this->model_webservice->validar_login_facebook($id_facebook);

					if ($dados) {

						$retornar = array(

							'id_usuario' => $dados->id_usuario,
							'nome_usuario' => $dados->nome_usuario,
							'email_usuario' => $dados->email_usuario,
							'senha_usuario' => $dados->senha_usuario,
							'grupo_usuario' => $dados->fk_grupo_usuario

						);

						if ($dados->fk_grupo_usuario != 6 && $dados->fk_grupo_usuario != 7) {
							
							$this->response(array('status' => 0, 'resultado' => 'Usuário sem permissão de acesso.'),Self::HTTP_UNAUTHORIZED); //401

						} else if($dados->ativo_usuario == 0) {

							$this->response(array('status' => 0, 'resultado' => 'Usuário inativo.'),Self::HTTP_UNAUTHORIZED); //401

						} else {

							if ($dados->fk_grupo_usuario == 6) {
								$this->response(array('status' => 1, 'resultado' => 'Login Motorista efetuado com sucesso','usuario' => $retornar),Self::HTTP_OK); //200
							} else if ($dados->fk_grupo_usuario == 7) {
								$this->response(array('status' => 1, 'resultado' => 'Login Cliente efetuado com sucesso','usuario' => $retornar),Self::HTTP_OK); //200
							} else {

							}

						}

					} else {
						$this->response(array('status' => 0, 'resultado' => 'Dados não localizados!'),Self::HTTP_NOT_FOUND); //404
					}


				} else {
					$this->response(array('status' => 0, 'resultado' => 'E-mail e/ou Senha não definidos ou incorretos.'),Self::HTTP_UNAUTHORIZED); //401
				}

			}
		
		}

		//Recuperar Senha
		public function esqueci_senha_get(){

			header('Content-Type: text/html; charset=utf-8');
			$valores = array('email_usuario' => $this->get('email_usuario'));

			$this->form_validation->set_data($valores);
			$this->form_validation->set_rules('email_usuario','Login do Usuário','required');

  			if ($this->form_validation->run()) {

				$caracteres = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0';
				$caractereVetor = explode(',',$caracteres);
				$senha = '';

				while(strlen($senha) < 10) { //Cria uma nova senha com 10 caracteres.
					
					$indice = mt_rand(0, count($caractereVetor) - 1);
					$senha .= $caractereVetor[$indice];

				}

				$this->model_usuarios->start();
				$email = $this->model_usuarios->senha_Email(($senha),$this->get('email_usuario'));

				//Usuário inativo ou desativado
				if($email == ""){
					$this->model_usuarios->rollback();
					$this->response(array('status' => 0, 'resultado' => 'Dados incorretos ou perfil inativo!'),Self::HTTP_NOT_FOUND); //404

				} else { //Senha enviada para o E-mail.

					// Detalhes do Email. 
					$this->email->from('uorkeuorke@gmail.com', 'Seleto | Troca de senha'); 
					$this->email->to($email); 
					$this->email->subject('Seleto | Troca de senha'); 
					$this->email->message('
				<html>
					<head>
					    <title>E-mail</title>
					    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
					    <style>
					        body{
					            font-family: \'Open Sans\', sans-serif;
					        }
					    </style>
					</head>
					<body>
					<p style="margin-top: 30px">&nbsp;</p>
					<p style="text-align: center"><img src="http://'.$_SERVER['HTTP_HOST'].base_url().'style_site/img/logo_rodape.png" width="130px"></p>
					<p style="margin-top: 30px">&nbsp;</p>
					<p style="width: 60%;margin: 0 auto; text-align: justify">

					<h1 style="text-align: center;font-weight: bolder;">Sua nova senha: '.$senha.'</h1>

					<p style="margin-top: 30px">&nbsp;</p>
					<p style="text-align: center;width: 320px;margin: 0 auto;font-weight: bolder;color: #696969">
					    Nova senha solicitada em: '.date('d/m/Y H:i:s').'
					</p>

					<p style="margin-top: 30px">&nbsp;</p>
					<h3 style="text-align: center;font-weight: bolder;">
					    Dúvidas?
					</h3 style="text-align: center">
					<p style="text-align: center;margin-top: -15px;font-weight: bolder;color: #696969">Entre em contato com: <span style="color:#47A9D6"> contato@seleto.com.br</span></p>
					<p style="text-align: center;color:#47A9D6;font-weight: bolder;">wwww.seleto.com.br</p>


					</p>
					</body>
				</html>');   

					// Enviar... 
					if ($this->email->send()) { 
						$commit = $this->model_usuarios->commit();

						if ($commit['status']) {
							$this->response(array('status' => 1, 'resultado' => "Nova Senha enviada para o E-mail: (".$email.")"),Self::HTTP_OK); //200
						} else {
							$this->response(array('status' => 0, 'resultado' => "Falha {$commit['erro']}"),Self::HTTP_BAD_REQUEST); //400
						}

					} else {
						$this->model_usuarios->rollback();
						$this->response(array('status' => 0, 'resultado' => "Erro ao enviar senha: ".$this->email->print_debugger()),Self::HTTP_BAD_REQUEST); //400

					}

				}


			} else { //Campos Preenchidos

				$erros = $this->post('email_usuario').strip_tags(validation_errors());

				$this->response(array('status' => 0, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400

			}  

		}

		//CRUD CLIENTE
		public function pre_cadastro_cliente_get(){

			$dados = $this->model_webservice->preCadastroCliente();

			$this->response(array('status' => 1, 
								  'resultado' => "Listas carregadas",
								  'sexo' => $dados['sexo'],
								  'pagamento' => $dados['pagamento'],
								  'operadoras' => $dados['operadoras'],
								  'estados' => $dados['estados']),Self::HTTP_OK); //200

		}

		public function novo_cliente_post(){

			header('Content-Type: text/html; charset=utf-8');

			$valores = array(
				'nome_usuario' => $this->post('nome_usuario'),
				'email_usuario' => $this->post('email_usuario'),
				'telefone_usuario' => $this->post('telefone_usuario'),
				'senha_usuario' => $this->post('senha_usuario'),
				'id_facebook' => $this->post('id_facebook'),
				'sexo_cliente' => $this->post('sexo_cliente'),
				'fk_grupo_usuario' => 7,//Cliente

				'cpf_cliente' => $this->post('cpf_cliente'),
				'identidade_cliente' => $this->post('identidade_cliente'),
				'pagamento_cliente' => $this->post('pagamento_cliente'),
				'rua_cliente' => $this->post('rua_cliente'),
				'numero_end_cliente' => $this->post('numero_end_cliente'),
				'complemento_cliente' => $this->post('complemento_cliente'),
				'bairro_cliente' => $this->post('bairro_cliente'),
				'cep_cliente' => $this->post('cep_cliente'),
				'cidade_cliente' => $this->post('cidade_cliente'),
				'estado_cliente' => $this->post('estado_cliente'),
				'operadora_cliente' => $this->post('operadora_cliente')
			);	
				

			$this->form_validation->set_data($valores);
			$this->form_validation->set_rules('nome_usuario',		'Nome','required');
			$this->form_validation->set_rules('email_usuario',		'E-mail','required|is_unique[seg_usuarios.email_usuario]');
			$this->form_validation->set_rules('telefone_usuario',	'Telefone','required');
			$this->form_validation->set_rules('senha_usuario',		'Senha','required');
			$this->form_validation->set_rules('sexo_cliente',		'Sexo','required');
			$this->form_validation->set_rules('cpf_cliente',		'CPF','required');
			$this->form_validation->set_rules('identidade_cliente',	'RG','required');
			$this->form_validation->set_rules('pagamento_cliente',	'Tipo Pagamento','required');
			$this->form_validation->set_rules('rua_cliente',		'Rua','required');
			$this->form_validation->set_rules('numero_end_cliente',	'Número','required');
			$this->form_validation->set_rules('bairro_cliente',		'Bairro','required');
			$this->form_validation->set_rules('cep_cliente',		'Cep','required');
			$this->form_validation->set_rules('cidade_cliente',		'Cidade','required');
			$this->form_validation->set_rules('estado_cliente',		'Estado','required');
			$this->form_validation->set_rules('operadora_cliente',	'Operadora','required');

			if ($this->form_validation->run()) {

				$this->model_webservice->start();
				$this->model_webservice->novoCliente($valores);
				$commit = $this->model_webservice->commit();

				if ($commit['status']){

					$this->response(array('status' => 1, 'resultado' => "Cliente cadastrado com sucesso"),Self::HTTP_OK); //200

				} else {

					$this->response(array('status' => 0, 'resultado' => 'Falha ao inserir dados. Log ('.$commit['log_erro'].')'),Self::HTTP_NOT_FOUND); //404

				}

			} else { //Campos Preenchidos

				$erros = strip_tags(validation_errors());
				$this->response(array('status' => 0, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400
			}

		}

		public function pre_edicao_cliente_get(){

			if ($this->autenticacao()) {

				$dados = $this->model_webservice->preEdicaoCliente($this->get('id_usuario'));

				$this->response(array('status' => 1, 
									  'resultado' => "Listas carregadas",

									  'dados' => $dados['selecionados'],
									  'sexo' => $dados['sexo'],
									  'pagamento' => $dados['pagamento'],
									  'operadoras' => $dados['operadoras'],
									  'estados' => $dados['estados']),
									  Self::HTTP_OK); //200

				} else {

					$this->response(array('status' => 0, 'resultado' => 'E-mail e/ou Senha não definidos ou incorretos.'),Self::HTTP_UNAUTHORIZED); //401

				}

		}

		public function editar_cliente_put(){

			header('Content-Type: text/html; charset=utf-8');

			$valores = array(
				'id_usuario' => $this->put('id_usuario'),
				'nome_usuario' => $this->put('nome_usuario'),
				'email_usuario' => $this->put('email_usuario'),
				'telefone_usuario' => $this->put('telefone_usuario'),
				'id_facebook' => $this->put('id_facebook'),
				'sexo_cliente' => $this->put('sexo_cliente'),
				'fk_grupo_usuario' => 7,//Cliente

				'cpf_cliente' => $this->put('cpf_cliente'),
				'identidade_cliente' => $this->put('identidade_cliente'),
				'pagamento_cliente' => $this->put('pagamento_cliente'),
				'rua_cliente' => $this->put('rua_cliente'),
				'numero_end_cliente' => $this->put('numero_end_cliente'),
				'complemento_cliente' => $this->put('complemento_cliente'),
				'bairro_cliente' => $this->put('bairro_cliente'),
				'cep_cliente' => $this->put('cep_cliente'),
				'cidade_cliente' => $this->put('cidade_cliente'),
				'estado_cliente' => $this->put('estado_cliente'),
				'operadora_cliente' => $this->put('operadora_cliente')
			);	

			$senha_ = $this->put('senha_usuario');
			if(isset($senha_) && $_SERVER['PHP_AUTH_PW'] != $senha_ && $senha_ != ""){

				$valores['senha_usuario'] = $senha_;

			} else {
				$valores['senha_usuario'] = $_SERVER['PHP_AUTH_PW']; //Mantem a mesma senha
			}

			$this->form_validation->set_data($valores);
			$this->form_validation->set_rules('nome_usuario',		'Nome','required');

			//E-mail alterado.
			if($_SERVER['PHP_AUTH_USER'] != $this->put('email_usuario')){
				//Validar se o novo e-mail é unico
				$this->form_validation->set_rules('email_usuario',		'E-mail','required|is_unique[seg_usuarios.email_usuario]');

			}
			
			$this->form_validation->set_rules('id_usuario',			'ID Usuário','required');
			$this->form_validation->set_rules('telefone_usuario',	'Telefone','required');
			$this->form_validation->set_rules('sexo_cliente',		'Sexo','required');
			$this->form_validation->set_rules('cpf_cliente',		'CPF','required');
			$this->form_validation->set_rules('identidade_cliente',	'RG','required');
			$this->form_validation->set_rules('pagamento_cliente',	'Tipo Pagamento','required');
			$this->form_validation->set_rules('rua_cliente',		'Rua','required');
			$this->form_validation->set_rules('numero_end_cliente',	'Número','required');
			$this->form_validation->set_rules('bairro_cliente',		'Bairro','required');
			$this->form_validation->set_rules('cep_cliente',		'Cep','required');
			$this->form_validation->set_rules('cidade_cliente',		'Cidade','required');
			$this->form_validation->set_rules('estado_cliente',		'Estado','required');
			$this->form_validation->set_rules('operadora_cliente',	'Operadora','required');

			if ($this->form_validation->run()) {

				$this->model_webservice->start();
				$this->model_webservice->editarCliente($valores);
				$commit = $this->model_webservice->commit();

				if ($commit['status']){

					$this->response(array('status' => 1, 'resultado' => "Cliente editado com sucesso"),Self::HTTP_OK); //200

				} else {

					$this->response(array('status' => 0, 'resultado' => 'Falha ao inserir dados. Log ('.$commit['log_erro'].')'),Self::HTTP_NOT_FOUND); //404

				}

			} else { //Campos Preenchidos

				$erros = strip_tags(validation_errors());
				$this->response(array('status' => 0, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400
			}

		}

		//CRUD MOTORISTA
		public function pre_cadastro_motorista_get(){

			$dados = $this->model_webservice->preCadastroMotorista();

			$this->response(array('status' => 1, 
								  'resultado' => "Listas carregadas",
								  'sexo' => $dados['sexo'],
								  'pagamento' => $dados['pagamento'],
								  'operadoras' => $dados['operadoras'],
								  'estados' => $dados['estados'],
								  'categorias' => $dados['categorias']
								  //'carros' => $dados['carros']
								  ),Self::HTTP_OK); //200

		}

		public function novo_motorista_post(){

			header('Content-Type: text/html; charset=utf-8');

			$valores = array(
				'nome_usuario' => $this->post('nome_usuario'),
				'email_usuario' => $this->post('email_usuario'),
				'telefone_usuario' => $this->post('telefone_usuario'),
				'senha_usuario' => $this->post('senha_usuario'),
				'id_facebook' => $this->post('id_facebook'),
				'sexo_motorista' => $this->post('sexo_motorista'),
				'fk_grupo_usuario' => 6,//motorista

				'cpf_motorista' => $this->post('cpf_motorista'),
				'identidade_motorista' => $this->post('identidade_motorista'),
				'pagamento_motorista' => $this->post('pagamento_motorista'),
				'rua_motorista' => $this->post('rua_motorista'),
				'numero_end_motorista' => $this->post('numero_end_motorista'),
				'complemento_motorista' => $this->post('complemento_motorista'),
				'bairro_motorista' => $this->post('bairro_motorista'),
				'cep_motorista' => $this->post('cep_motorista'),
				'cidade_motorista' => $this->post('cidade_motorista'),
				'estado_motorista' => $this->post('estado_motorista'),
				'operadora_motorista' => $this->post('operadora_motorista'),

				//'fk_carro' 			=> $this->post('fk_carro'),
				'cnh_num_motorista' => $this->post('cnh_num_motorista'),
				'cnh_cat_motorista' => $this->post('cnh_cat_motorista'),
				'cnh_validade' => $this->post('cnh_validade')
				


			);	
				

			$this->form_validation->set_data($valores);
			$this->form_validation->set_rules('nome_usuario',		'Nome','required');
			$this->form_validation->set_rules('email_usuario',		'E-mail','required|is_unique[seg_usuarios.email_usuario]');
			$this->form_validation->set_rules('telefone_usuario',	'Telefone','required');
			$this->form_validation->set_rules('senha_usuario',		'Senha','required');
			$this->form_validation->set_rules('sexo_motorista',		'Sexo','required');
			$this->form_validation->set_rules('cpf_motorista',		'CPF','required');
			$this->form_validation->set_rules('identidade_motorista',	'RG','required');
			$this->form_validation->set_rules('pagamento_motorista',	'Tipo Pagamento','required');
			$this->form_validation->set_rules('rua_motorista',		'Rua','required');
			$this->form_validation->set_rules('numero_end_motorista',	'Número','required');
			$this->form_validation->set_rules('bairro_motorista',		'Bairro','required');
			$this->form_validation->set_rules('cep_motorista',		'Cep','required');
			$this->form_validation->set_rules('cidade_motorista',		'Cidade','required');
			$this->form_validation->set_rules('estado_motorista',		'Estado','required');
			$this->form_validation->set_rules('operadora_motorista',	'Operadora','required');

			//$this->form_validation->set_rules('fk_carro',	'Carro','required');
			$this->form_validation->set_rules('cnh_num_motorista',	'Número CNH','required');
			$this->form_validation->set_rules('cnh_cat_motorista',	'Categoria','required');
			$this->form_validation->set_rules('cnh_validade',	'Validade','required');

			if ($this->form_validation->run()) {

				$this->model_webservice->start();
				$this->model_webservice->novoMotorista($valores);
				$commit = $this->model_webservice->commit();

				if ($commit['status']){

					$this->response(array('status' => 1, 'resultado' => "Motorista cadastrado com sucesso"),Self::HTTP_OK); //200

				} else {

					$this->response(array('status' => 0, 'resultado' => 'Falha ao inserir dados. Log ('.$commit['log_erro'].')'),Self::HTTP_NOT_FOUND); //404

				}

			} else { //Campos Preenchidos

				$erros = strip_tags(validation_errors());
				$this->response(array('status' => 0, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400
			}

		}

		public function pre_edicao_motorista_get(){

			if ($this->autenticacao()) {

				$dados = $this->model_webservice->preEdicaoMotorista($this->get('id_usuario'));

				$this->response(array('status' => 1, 
									  'resultado' => "Listas carregadas",

									  'dados' => $dados['selecionados'],
									  'sexo' 		 => $dados['sexo'],
									  'pagamento' 	 => $dados['pagamento'],
									  'operadoras' 	 => $dados['operadoras'],
									  'estados' 	 => $dados['estados'],
									  'categorias'   => $dados['categorias']
								  	  //'carros' => $dados['carros']
								  	),Self::HTTP_OK); //200

				} else {

					$this->response(array('status' => 0, 'resultado' => 'E-mail e/ou Senha não definidos ou incorretos.'),Self::HTTP_UNAUTHORIZED); //401

				}

		}

		public function editar_motorista_put(){

			header('Content-Type: text/html; charset=utf-8');
			if ($this->autenticacao()) {

				$valores = array(

					'id_usuario' => $this->put('id_usuario'),
					'nome_usuario' => $this->put('nome_usuario'),
					'email_usuario' => $this->put('email_usuario'),
					'telefone_usuario' => $this->put('telefone_usuario'),
					'senha_usuario' => $this->put('senha_usuario'),
					'id_facebook' => $this->put('id_facebook'),
					'fk_grupo_usuario' => 6,//motorista

					'sexo_motorista' => $this->put('sexo_motorista'),
					'cpf_motorista' => $this->put('cpf_motorista'),
					'identidade_motorista' => $this->put('identidade_motorista'),
					'pagamento_motorista' => $this->put('pagamento_motorista'),
					'rua_motorista' => $this->put('rua_motorista'),
					'numero_end_motorista' => $this->put('numero_end_motorista'),
					'complemento_motorista' => $this->put('complemento_motorista'),
					'bairro_motorista' => $this->put('bairro_motorista'),
					'cep_motorista' => $this->put('cep_motorista'),
					'cidade_motorista' => $this->put('cidade_motorista'),
					'estado_motorista' => $this->put('estado_motorista'),
					'operadora_motorista' => $this->put('operadora_motorista'),

					//'fk_carro' => $this->put('fk_carro'),
					'cnh_num_motorista' => $this->put('cnh_num_motorista'),
					'cnh_cat_motorista' => $this->put('cnh_cat_motorista'),
					'cnh_validade' => $this->put('cnh_validade')
					
				);	
					
				$senha_ = $this->put('senha_usuario');
				if(isset($senha_) && $_SERVER['PHP_AUTH_PW'] != $senha_ && $senha_ != ""){

					$valores['senha_usuario'] = $senha_;

				} else {
					$valores['senha_usuario'] = $_SERVER['PHP_AUTH_PW']; //Mantem a mesma senha
				}


				$this->form_validation->set_data($valores);
				$this->form_validation->set_rules('id_usuario',		'id','required');
				$this->form_validation->set_rules('nome_usuario',		'Nome','required');
			
				//E-mail alterado.
				if($_SERVER['PHP_AUTH_USER'] != $this->put('email_usuario')){
					//Validar se o novo e-mail é unico
					$this->form_validation->set_rules('email_usuario',		'E-mail','required|is_unique[seg_usuarios.email_usuario]');

				}

				$this->form_validation->set_rules('telefone_usuario',	'Telefone','required');
				$this->form_validation->set_rules('sexo_motorista',		'Sexo','required');
				$this->form_validation->set_rules('cpf_motorista',		'CPF','required');
				$this->form_validation->set_rules('identidade_motorista',	'RG','required');
				$this->form_validation->set_rules('pagamento_motorista',	'Tipo Pagamento','required');
				$this->form_validation->set_rules('rua_motorista',		'Rua','required');
				$this->form_validation->set_rules('numero_end_motorista',	'Número','required');
				$this->form_validation->set_rules('bairro_motorista',		'Bairro','required');
				$this->form_validation->set_rules('cep_motorista',		'Cep','required');
				$this->form_validation->set_rules('cidade_motorista',		'Cidade','required');
				$this->form_validation->set_rules('estado_motorista',		'Estado','required');
				$this->form_validation->set_rules('operadora_motorista',	'Operadora','required');

				//$this->form_validation->set_rules('fk_carro',	'Carro','required');
				$this->form_validation->set_rules('cnh_num_motorista',	'Número CNH','required');
				$this->form_validation->set_rules('cnh_cat_motorista',	'Categoria','required');
				$this->form_validation->set_rules('cnh_validade',	'Validade','required');

				if ($this->form_validation->run()) {

					$this->model_webservice->start();
					$this->model_webservice->editarMotorista($valores);
					$commit = $this->model_webservice->commit();

					if ($commit['status']){

						$this->response(array('status' => 1, 'resultado' => "Motorista editado com sucesso"),Self::HTTP_OK); //200

					} else {

						$this->response(array('status' => 0, 'resultado' => 'Falha ao editar dados. Log ('.$commit['log_erro'].')'),Self::HTTP_NOT_FOUND); //404

					}
				} else { //Campos Não Preenchidos

					$erros = strip_tags(validation_errors());
					$this->response(array('status' => 0, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400
				}

			} else {

				$this->response(array('status' => 0, 'resultado' => 'E-mail e/ou Senha não definidos ou incorretos'),Self::HTTP_UNAUTHORIZED); //401

			}

		}

		public function notifica_deslocamento_put(){

			header('Content-Type: text/html; charset=utf-8');
			
			if ($this->autenticacao()) {
				$dados = array(
					'id_motorista' => $this->put('id_motorista'),
					'latitude_localizacao' => $this->put('latitude'),
					'longitude_localizacao' => $this->put('longitude'),
					'hora_localizacao' => date('Y-m-d H:i:s')
					);
				$this->form_validation->set_data($dados);
				$this->form_validation->set_rules('id_motorista', 'ID do motorista', 'required');
				$this->form_validation->set_rules('latitude_localizacao', 'Latitude', 'required');
				$this->form_validation->set_rules('longitude_localizacao', 'Longitude', 'required');

				if($this->form_validation->run()) {
					
					$this->model_webservice->start();
					echo $this->model_webservice->notificaDeslocamento($dados);
					$commit = $this->model_webservice->commit();

					if ($commit['status']){

						$this->response(array('status' => 1, 'resultado' => "Notificação enviada"),Self::HTTP_OK); //200

					} else {

						$this->response(array('status' => 0, 'resultado' => 'Falha ao enviar notificação. Log ('.$commit['log_erro'].')'),Self::HTTP_NOT_FOUND); //404

					}
				} else { //Campos Não Preenchidos

					$erros = strip_tags(validation_errors());
					$this->response(array('status' => 0, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400
				}
			}
		}

		/*Lista dos carros WS11*/
		public function pre_corrida_get(){

			if ($this->autenticacao()) {
				$valores = array('id_usuario' => $this->get('id_usuario'));

				$this->form_validation->set_data($valores);
				$this->form_validation->set_rules('id_usuario',	'ID','required');

				if ($this->form_validation->run()) {
					$dados = $this->model_webservice->preCorrida($valores);

					$this->response(array('status' => 1, 
										  'resultado' => "Lista carregada",
										  	'carros' => $dados),
										  	Self::HTTP_OK); //200
				} else {

					$erros = strip_tags(validation_errors());
					$this->response(array('status' => 0, 'resultado' => str_replace("\r\n","",$erros)),Self::HTTP_BAD_REQUEST); //400

				}

			} else { //Falha Autenticação

				$this->response(array('status' => 0, 'resultado' => 'E-mail e/ou Senha não definidos ou incorretos.'),Self::HTTP_UNAUTHORIZED); //401

			}

		}

	}