<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_site extends CI_Controller {

	public function index(){

		$this->load->view('site/topo');
		$this->load->view('site/index');
		$this->load->view('site/rodape');

	}

	public function sobre(){

		$this->load->view('site/topo');
		$this->load->view('site/sobre');
		$this->load->view('site/rodape');

	}

}