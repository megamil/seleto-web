/*
Observações.
	Comentários: Os campos em tabelas nas quais os usuários podem alterar seu valor devem ser comentados, 
	pois o comentário será usado no relatório de edições realizadas, para melhor identificar o campo que recebeu 
	a alteração.

	Alterar:
		Em Drop, e Create o nome do schema.
		Em view_relatorio_edicoes alterar o nome do schema
*/

DROP DATABASE naville_seleto;
CREATE SCHEMA naville_seleto; /*ALTERAR NO VIEW_ALIAS ANTES DE USAR.*/
USE naville_seleto;

/*
u676502882_selet
dev123
*/

/*Segurança Usuários*/
CREATE TABLE IF NOT EXISTS seg_usuarios(

	id_usuario int not null AUTO_INCREMENT comment "ID Usuário",
	nome_usuario character varying(100) NOT NULL comment "Nome Usuário",
	email_usuario character varying(100) NOT NULL comment "E-mail Usuário",
	telefone_usuario character varying(11) NOT NULL comment "Telefone Usuário",
	login_usuario character varying(100) NOT NULL comment "Login Usuário",
	senha_usuario character varying(40) NOT NULL comment "Senha Usuário",
	ativo_usuario boolean NOT NULL comment "Status Usuário",
	fk_grupo_usuario int not null comment "Grupo Usuário",
	id_facebook text comment "ID Facebook",
	token_facebook text comment "Token Facebook",
	usuario_criou_usuario int comment "Usuário que criou",
	criacao_usuario timestamp DEFAULT CURRENT_TIMESTAMP comment "Data criação",

	PRIMARY KEY (id_usuario),
	UNIQUE (email_usuario),
	UNIQUE (login_usuario),
	FOREIGN KEY (usuario_criou_usuario) REFERENCES seg_usuarios (id_usuario)

);

/*Segurança Grupos*/
CREATE TABLE IF NOT EXISTS seg_grupos(

	id_grupo int not null AUTO_INCREMENT comment "ID Grupo",
	nome_grupo character varying(100) NOT NULL comment "Nome Grupo",
	descricao_grupo text NOT NULL comment "Descrição Grupo",
	usuario_criou_grupo int comment "Usuário que criou",
	ativo_grupo boolean NOT NULL comment "Status Grupo",
	criacao_grupo timestamp DEFAULT CURRENT_TIMESTAMP comment "Data Criação",

	PRIMARY KEY (id_grupo),
	CONSTRAINT unique_nome_grupo UNIQUE (nome_grupo),
	FOREIGN KEY (usuario_criou_grupo) REFERENCES seg_usuarios (id_usuario)

);

ALTER TABLE seg_usuarios
ADD FOREIGN KEY (fk_grupo_usuario) REFERENCES seg_grupos(id_grupo);

/*Segurança, cadastro dos models*/
CREATE TABLE IF NOT EXISTS seg_models(

	id_model int not null AUTO_INCREMENT,
	link_model character varying(100) not null,
	descricao_model text not null,

	CONSTRAINT pk_model PRIMARY KEY (id_model)

);

CREATE TABLE IF NOT EXISTS seg_log_acesso(

	id_log_acesso int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_acesso timestamp DEFAULT CURRENT_TIMESTAMP,
	ip_usuario_acesso character varying(16),
	acesso boolean,
	maquina_usuario_acesso text,

	PRIMARY KEY (id_log_acesso),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

CREATE TABLE IF NOT EXISTS seg_log_erro(

	id_log_erro int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_erro timestamp DEFAULT CURRENT_TIMESTAMP,
	cod text, /*Text para casos onde o código seja texto ao invés de número*/
	erro text,
	query text,
	erro_feedback text, /*Relatado pelo usuário*/
	funcao text,
	maquina_usuario_erro text,

	PRIMARY KEY (id_log_erro),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

/*Segurança, cadastro dos controllers*/
CREATE TABLE IF NOT EXISTS seg_controllers(

	id_controller int not null AUTO_INCREMENT,
	link_controller character varying(100) not null,
	descricao_controller text not null,
	fk_model int,

	PRIMARY KEY (id_controller),
	FOREIGN KEY (fk_model) REFERENCES seg_models (id_model)

);

/*Segurança menu, gerá um menu no aplicativo, as telas devem ser vinculadas*/
CREATE TABLE IF NOT EXISTS seg_menu(

	id_menu int not null AUTO_INCREMENT,
	titulo_menu character varying(100) NOT NULL,
	descricao_menu text NOT NULL,
	menu_acima int, 
	posicao_menu int,

	PRIMARY KEY (id_menu),
	FOREIGN KEY (menu_acima) REFERENCES seg_menu (id_menu)

);

/*Segurança, aplicações do menu.*/
CREATE TABLE IF NOT EXISTS seg_aplicacao(

	id_aplicacao int not null AUTO_INCREMENT,
	link_aplicacao character varying(100) NOT NULL,
	titulo_aplicacao character varying(100) NOT NULL,
	descricao_aplicacao text NOT NULL,
	fk_controller int,

	PRIMARY KEY (id_aplicacao),
    FOREIGN KEY (fk_controller) REFERENCES seg_controllers (id_controller)

);

/*Gera um Log dos acessos do usuário.*/
CREATE TABLE IF NOT EXISTS seg_log_navegacao(

	id_log_navegacao int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_navegacao timestamp DEFAULT CURRENT_TIMESTAMP,
	permissao boolean,
	fk_aplicacao int,
	parametros text,

	PRIMARY KEY (id_log_navegacao),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao)

);

CREATE TABLE IF NOT EXISTS seg_log_edicao(

	id_log_edicao int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_edicao timestamp DEFAULT CURRENT_TIMESTAMP,
	original_edicao text,
	novo_edicao text,
	campo_edicao text,
	tabela_edicao text,
	fk_aplicacao int,
	id_edicao int,
	

	PRIMARY KEY (id_log_edicao),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao)

);

/*Vincula as aplicações ao menu*/
CREATE TABLE IF NOT EXISTS seg_aplicacoes_menu(

	id_aplicacoes_menu int not null AUTO_INCREMENT,
	fk_aplicacao int NOT NULL,
	fk_menu int NOT NULL,

	PRIMARY KEY (id_aplicacoes_menu),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao),
    FOREIGN KEY (fk_menu) REFERENCES seg_menu (id_menu)

);

/*Segurança, Grupo aplicação*/
CREATE TABLE IF NOT EXISTS seg_aplicacoes_grupos(

	id_aplicacoes_grupos int not null AUTO_INCREMENT,
	fk_grupo int NOT NULL,
	fk_aplicacao int NOT NULL,

	PRIMARY KEY (id_aplicacoes_grupos),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao),
    FOREIGN KEY (fk_grupo) REFERENCES seg_grupos (id_grupo)

);

/*Cadastro dos controllers e models*/
insert into seg_models (id_model,link_model,descricao_model) values 
	(1,'Model_menu','Responsável pelos menus.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(2,'Model_seguranca','Responsável pelo acesso as sistema e controle de perfils.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(3,'Model_usuarios','Responsável pelo gerênciamento dos usuários.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(4,'Model_grupos','Responsável pelo gerênciamento dos grupos.');


insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(1,'Main','Responsável pelo gerênciamento do acesso ao sistema.',2);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(2,'Controller_usuarios','Responsável pelo gerênciamento dos usuários.',3);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(3,'Controller_grupos','Responsável pelo gerênciamento dos grupos.',4);


/*INSERTS PARA USUÁRIO DE TESTES*/
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (1,'Administradores','Administradores do sistema',true);
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (2,'Autorizado','Autorizado',true);
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (3,'Mensagem','Mensagem',true);
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (4,'Consulta','Consulta',true);
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (5,'Cadastros','Cadastros',true);
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (6,'Motorista','Motorista',true);
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (7,'Clientes','Clientes',true);


insert into seg_usuarios (id_usuario,nome_usuario,email_usuario,telefone_usuario,login_usuario,senha_usuario,ativo_usuario,fk_grupo_usuario)
values (1,'Usuário Administrador','megamil3d@gmail.com','11962782329','admin','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,7);

/*Definindo os menus.*/
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (1,'Segurança','Segurança, Perfils e Grupos',null,1000);	 /*1000 para Garantir que será o útima menu*/
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (2,'Editar Perfil','Editar Perfil',1,null);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (3,'Usuários','Lista de usuários',1,null);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (4,'Grupos','Lista de grupos',1,null);

/*Difinindo aplicações e links*/
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (1,'seguranca/view_editar_perfil','Editar Perfil','Editar Perfil',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (2,'seguranca/view_usuarios','Listar Usuários','Listar Usuários',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (3,'seguranca/view_grupos','Listar Grupos','Listar Grupos',3);

/*Aplicações sem menu*/
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (4,'seguranca/view_editar_grupo','Editar Grupo','Editar Grupo',3);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (5,'seguranca/view_editar_usuario','Editar Usuário','Editar Usuários',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (6,'seguranca/view_novo_grupo','Novo Grupo','Novo Grupo',3);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (7,'seguranca/view_novo_usuario','Novo Usuário','Novo Usuários',2);

/*Indicando quais aplicações estão ligadas a quais menus.*/
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (1,2);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (2,3);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (3,4);

/*Dando permissões para o grupo administrador.*/
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(1,1); 
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(2,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(3,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(4,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(5,1); 
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(6,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(7,1);

/*Notificações*/
INSERT INTO seg_models (id_model,link_model, descricao_model) VALUES (5,'Model_notificacoes', 'Model Notificações');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (4,'Controller_notificacoes', 'Controller Notificações', 5);

INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (NULL, 999, 'Notificações', 'Listas de Notificações', 5);
INSERT INTO seg_menu (menu_acima, posicao_menu, titulo_menu, descricao_menu, id_menu) VALUES (1, NULL, 'Enviar Notificação', 'Lista de suas notificações podendo enviar uma nova.', 6);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (8,'notificacoes/view_notificacoes', 'Notificações', 'Notificações', 4);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (8,5);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(8,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (9,'notificacoes/view_hist_notificacoes', 'Minhas Notificações', 'Minhas Notificações', 4);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (9,6);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(9,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (10,'notificacoes/view_notificar', 'Nova Notificação', 'Nova Notificação', 4);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(10,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (11,'notificacoes/view_editar_notificacao', 'Editar Notificação', 'Editar Notificação', 4);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(11,1);

/*Relatórios*/
INSERT INTO seg_models (id_model,link_model, descricao_model) VALUES (6,'Model_relatorios', 'Model Relatórios');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (5,'Controller_relatorios', 'Relatórios', 6);

INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (NULL, 'Relatórios', 'Relatórios', 7, 998);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (7, 'Acessos', 'Relatório de Acessos', 8, null);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (7, 'Navegação', 'Relatório de Navegação', 9, null);
INSERT INTO seg_menu (menu_acima, titulo_menu, descricao_menu, id_menu, posicao_menu) VALUES (7, 'Edições', 'Relatório de Edições', 10, null);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (12,'relatorios/view_relatorio_acesso', 'Acessos', 'Relatório dos Acessos', 5);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (12,8);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(12,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (13,'relatorios/view_relatorio_navegacao', 'Navegação', 'Relatório de Navegação', 5);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (13,9);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(13,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (14,'relatorios/view_relatorio_edicoes', 'Edições', 'Relatório de Edições', 5);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu) values (14,10);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(14,1);

CREATE TABLE IF NOT EXISTS cad_notificacao (

	id_notificacao int not null AUTO_INCREMENT,
	fk_usuario int,
	data_notificacao timestamp DEFAULT CURRENT_TIMESTAMP,
	titulo_notificacao character varying(20),
	notificacao text,
	data_limite_notificacao date,

	PRIMARY KEY (id_notificacao),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

CREATE TABLE IF NOT EXISTS cad_hist_notificacao (

	id_hist_notificacao int not null AUTO_INCREMENT,
	fk_usuario_destino int,
	fk_notificacao int,
	data_envio_notificacao timestamp DEFAULT CURRENT_TIMESTAMP,
	data_leitura timestamp DEFAULT 0,
	notificacao_enviada boolean DEFAULT false, /*Quando o navegador/mobile receber já marca para não enviar novamente.*/

	PRIMARY KEY (id_hist_notificacao),
	FOREIGN KEY (fk_notificacao) REFERENCES cad_notificacao (id_notificacao),
	FOREIGN KEY (fk_usuario_destino) REFERENCES seg_usuarios (id_usuario)

);


/*Views*/
-- Por não trazer os comentários das views não pode ajudar.
-- create or replace view view_alias as
-- 	SELECT TABLE_NAME as tabela,
-- 		   COLUMN_KEY as chave,
-- 		   COLUMN_NAME as coluna,
-- 		   DATA_TYPE as tipo,
-- 		   CHARACTER_MAXIMUM_LENGTH as tamanho,
-- 		   COLUMN_TYPE as coluna_detalhes,
-- 		   COLUMN_COMMENT as comentario
-- 	FROM INFORMATION_SCHEMA.COLUMNS
-- 	WHERE TABLE_SCHEMA = 'naville_seleto';

/*Tabela que irá dar detalhes dos campos da view*/
CREATE TABLE IF NOT EXISTS cad_detalhes_views (

	id_detalhes_views  int not null AUTO_INCREMENT,
	nome_view text,
	nome_campo text,
	tipo_campo text,
	descricao_campo text,
	visivel boolean DEFAULT true, /*Existem campos que não devem ser listados, mas existem para serem usados de filtro*/
	
	PRIMARY KEY (id_detalhes_views)

);
/***************************************************************************************************/
create or replace view view_relatorio_acessos as
	SELECT  
		id_log_acesso as id,
		id_usuario,
		nome_usuario as usuario,
		data_log_acesso as data_acesso,
		date_format(data_log_acesso,'%d/%m/%Y as  %H:%i:%s') as data_acesso_formatado,
		ip_usuario_acesso as ip,
		maquina_usuario_acesso as maquina,
		acesso, 

		case acesso 
			when 1 then 'Entrou' 
            when 0 then 'Saiu' end as acessou
            
		FROM seg_log_acesso
		inner join seg_usuarios on id_usuario = fk_usuario;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values 
	("view_relatorio_acessos","id","int","ID",true),
	("view_relatorio_acessos","usuario","text","Nome Usuário",true),
	("view_relatorio_acessos","data_acesso_formatado","timestamp","Data Acesso",true),
	("view_relatorio_acessos","ip","text","IP",true),
	("view_relatorio_acessos","maquina","text","Maquina / Navegador",true),
	("view_relatorio_acessos","acessou","text","Login / Logoff",true),
	-- Campos usados somente para o where.
	("view_relatorio_acessos","id_usuario","int","Nome Usuário",false),
	("view_relatorio_acessos","data_acesso","timestamp","Data Acesso",false),
	("view_relatorio_acessos","acesso","int","Login / Logoff",false);

/***************************************************************************************************/

create or replace view view_relatorio_navegacao as
	select 
    id_log_navegacao as id,
    fk_usuario,
    nome_usuario,
    data_log_navegacao as data_log,
    date_format(data_log_navegacao,'%d/%m/%Y as  %H:%i:%s') as data_log_formatado,
    permissao,
    case permissao 
			when 1 then 'Sim' 
            when 0 then 'Não' end as com_permissao,
	fk_aplicacao,
    descricao_aplicacao,
    parametros
    
    from seg_log_navegacao
    inner join seg_usuarios on fk_usuario = id_usuario
    inner join seg_aplicacao on id_aplicacao = fk_aplicacao;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values 
	("view_relatorio_navegacao","id","int","ID",true),
	("view_relatorio_navegacao","nome_usuario","text","Nome Usuário",true),
	("view_relatorio_navegacao","data_log_formatado","timestamp","Data Acesso",true),
	("view_relatorio_navegacao","com_permissao","timestamp","Tem acesso?",true),
	("view_relatorio_navegacao","descricao_aplicacao","text","Aplicação",true),
	("view_relatorio_navegacao","parametros","text","Parametros Usados",true),

	("view_relatorio_navegacao","fk_usuario","int","Nome Usuário",false),
	("view_relatorio_navegacao","data_log","timestamp","Data Acesso",false),
	("view_relatorio_navegacao","permissao","tinyint","Tem acesso?",false),
	("view_relatorio_navegacao","fk_aplicacao","int","Aplicação",false);
/***************************************************************************************************/
create or replace view view_relatorio_edicoes as
	select 
    id_log_edicao as id,
    fk_usuario,
    nome_usuario,
    data_log_edicao as data_log,
    date_format(data_log_edicao,'%d/%m/%Y as  %H:%i:%s') as data_log_formatado,
    original_edicao,
    novo_edicao,
    fk_aplicacao,
    descricao_aplicacao,
    id_edicao,
    (SELECT  COLUMN_COMMENT as comentario
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = 'naville_seleto' AND TABLE_NAME = tabela_edicao AND COLUMN_NAME = campo_edicao) as campo
    
    from seg_log_edicao
    inner join seg_usuarios on fk_usuario = id_usuario
    inner join seg_aplicacao on id_aplicacao = fk_aplicacao;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values 
	("view_relatorio_edicoes","id","int","ID",true),
	("view_relatorio_edicoes","nome_usuario","text","Nome Usuário",true),
	("view_relatorio_edicoes","data_log_formatado","timestamp","Data Edição",true),
	("view_relatorio_edicoes","original_edicao","text","Valor Original",true),
	("view_relatorio_edicoes","novo_edicao","text","Novo Valor",true),
	("view_relatorio_edicoes","descricao_aplicacao","text","Titulo Aplicação",true),
	("view_relatorio_edicoes","id_edicao","int","Filtro Edição",true),
	("view_relatorio_edicoes","campo","text","Campo",true),

	("view_relatorio_edicoes","fk_usuario","int","Nome Usuário",false),
	("view_relatorio_edicoes","data_log","timestamp","Data Edição",false),
	("view_relatorio_edicoes","fk_aplicacao","int","Titulo Aplicação",false);
/***************************************************************************************************/
/*View de listas*/
create or replace view view_lista_grupos as
	select 
    id_grupo as id,
    nome_grupo,
    descricao_grupo,
    usuario_criou_grupo as usuario,
    nome_usuario,
    ativo_grupo,
    case ativo_grupo 
			when 1 then 'Ativo' 
            when 0 then 'Inativo' end as ativo,
    criacao_grupo,
    date_format(criacao_grupo,'%d/%m/%Y as  %H:%i:%s') as criacao_grupo_formatado,
    (select count(*) from seg_usuarios where fk_grupo_usuario = id_grupo and ativo_usuario = true) as usuarios_ativos
    
    from seg_grupos
    left join seg_usuarios on id_usuario = usuario_criou_grupo;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values 
	("view_lista_grupos","id","int","ID",true),
	("view_lista_grupos","nome_grupo","text","Nome Grupo",true),
	("view_lista_grupos","descricao_grupo","text","Descrição Grupo",true),
	("view_lista_grupos","nome_usuario","text","Criado por",true),
	("view_lista_grupos","criacao_grupo_formatado","timestamp","Criado Em",true),
	("view_lista_grupos","usuarios_ativos","int","Usuários Ativos",true),
	("view_lista_grupos","ativo","tinyint","Status Grupo",true),

	("view_lista_grupos","usuario","int","Criado por",false),
	("view_lista_grupos","ativo_grupo","tinyint","Status Grupo",false),
	("view_lista_grupos","criacao_grupo","timestamp","Criado Em",false);
/***************************************************************************************************/
create or replace view view_lista_usuarios as
	select 
    id_usuario as id,
    nome_usuario,
    email_usuario,
    telefone_usuario,
    login_usuario,
    ativo_usuario,
    case ativo_usuario 
			when 1 then 'Sim' 
            when 0 then 'Não' end as ativo,
    fk_grupo_usuario,
    nome_grupo,
    usuario_criou_usuario usuario,
    (select nome_usuario from seg_usuarios su0 where su0.id_usuario = su1.usuario_criou_usuario) as usuario_criou,
    criacao_usuario,
    date_format(criacao_usuario,'%d/%m/%Y as  %H:%i:%s') as criacao_usuario_formatado
    
    from seg_usuarios su1
    inner join seg_grupos on id_grupo = fk_grupo_usuario;

insert into cad_detalhes_views (nome_view,nome_campo,tipo_campo,descricao_campo,visivel) values 
	("view_lista_usuarios","id","int","ID",true),
	("view_lista_usuarios","nome_usuario","text","Usuário",true),
	("view_lista_usuarios","email_usuario","text","E-mail",true),
	("view_lista_usuarios","telefone_usuario","text","Telefone",true),
	("view_lista_usuarios","login_usuario","text","Login",true),
	("view_lista_usuarios","ativo","text","Status Usuário",true),
	("view_lista_usuarios","nome_grupo","text","Grupo",true),
	("view_lista_usuarios","usuario_criou","text","Criado por",true),
	("view_lista_usuarios","criacao_usuario_formatado","timestamp","Criado em",true),
	
	("view_lista_usuarios","ativo_usuario","tinyint","Status Usuário",false),
	("view_lista_usuarios","fk_grupo_usuario","int","Grupo",false),
	("view_lista_usuarios","usuario","int","Criado por",false),
	("view_lista_usuarios","criacao_usuario","timestamp","Criado em",false);
/***************************************************************************************************/
/*Seleto*/

/*GRUPO DE ITENS*/
CREATE TABLE IF NOT EXISTS cad_grupos(

	id_grupo int not null AUTO_INCREMENT,
	nome_grupo text,

	PRIMARY KEY (id_grupo)

);

	insert into cad_grupos (id_grupo,nome_grupo)
				values 
					(1,'Sexo'),
					(2,'Forma de Pagamento'),
					(3,'Operadoras'),
					(4,'Estados'),
					(5,'Categorias CNH'),
					(6, 'Status Motorista'),
					(7, 'Status Cliente'),
					(8, 'Status Corrida');

CREATE TABLE IF NOT EXISTS cad_item_grupo(

	id_item_grupo int not null AUTO_INCREMENT,
	fk_grupo int,
	nome_item_grupo text,

	PRIMARY KEY (id_item_grupo),
	FOREIGN KEY (fk_grupo) REFERENCES cad_grupos (id_grupo)

);

	insert into cad_item_grupo (fk_grupo,nome_item_grupo) 
				values 
					(1,'Masculino'), /*Sexo*/
					(1,'Feminino'),

					(2,'Dinheiro'), /*Forma de Pagamento*/
					(2,'Cartão Crédito'),
					(2,'Cartão Débito'),
					(2,'Todas Anteriores'),

					(3,'Claro'), /*Operadoras*/
					(3,'OI'),
					(3,'TIM'),
					(3,'Vivo'),

					(4,'AC'), /*Estados*/
					(4,'AL'),
					(4,'AP'),
					(4,'AM'),
					(4,'BA'),
					(4,'CE'),
					(4,'DF'),
					(4,'ES'),
					(4,'GO'),
					(4,'MA'),
					(4,'MT'),
					(4,'MS'),
					(4,'MG'),
					(4,'PA'),
					(4,'PB'),
					(4,'PR'),
					(4,'PE'),	
					(4,'PI'),
					(4,'RJ'),
					(4,'RN'),
					(4,'RS'),
					(4,'RO'),
					(4,'RR'),
					(4,'SC'),
					(4,'SP'),
					(4,'SE'),
					(4,'TO'),

					(5,'A'), /*Categorias CNH*/
					(5,'B'),
					(5,'AB'),
					(5,'C'),
					(5,'D'),
					(5,'E'),
					
					(6, 'Ativo'), /*Status Motorista*/
					(6, 'Bloqueado'),
					(6, 'Em análise'),
					(6, 'Cancelado'),

					(7, 'Ativo'), /*Status Cliente*/
					(7, 'Inativo'),

					(8, 'Aguardando'), /*Status Corrida*/
					(8, 'Em Andamento'),
					(8, 'Cancelada Cliente'),
					(8, 'Cancelada Motorista'),
					(8, 'Encerrada');

/*GRUPO DE ITENS*/
CREATE TABLE IF NOT EXISTS cad_tipo_carro(

	id_tipo_carro int not null AUTO_INCREMENT,
	tipo_carro text,
	descricao_carro text,
	tarifa_carro real,
	mensalidade_carro int,
	ativo_carro boolean,
	pink_carro boolean, 

	PRIMARY KEY (id_tipo_carro)

);

insert into cad_tipo_carro (id_tipo_carro,tipo_carro,descricao_carro,tarifa_carro,mensalidade_carro,ativo_carro,pink_carro)
	values 
		(1,'Ultra Econômico','Carro sem ar condicionado',1.25,159.99,1,0),
		(2,'Econômico','Carro com ar condicionado',1.5,159.99,1,0),
		(3,'Premium','Carros de luxo',3.0,159.99,1,0),
		(4,'Pink Ultra Econômico','Motorista Mulher sem ar condicionado',1.25,159.99,1,1),
		(5,'Pink Econômico','Motorista Mulher',1.5,159.99,1,1),
		(6,'Pink Premium','Motorista Mulher carro de luxo',3.0,159.99,1,1),
		(7,'Caminhonete','',2.30,159.99,0,0), /*Solicitado ocultar*/
		(8,'Moto Taxi','Moto Taxistas',0.75,89.99,1,0),
		(9,'Moto Frete','Entrega de pequenos objetos',0.90,89.99,1,0);

/*Dados*/
CREATE TABLE IF NOT EXISTS cad_cliente(

	id_cliente int not null AUTO_INCREMENT,
	fk_usuario int,
	sexo_cliente int,
	cpf_cliente character varying(11),
	identidade_cliente character varying(10),
	pagamento_cliente int,
	fk_status int DEFAULT 48,

	rua_cliente text,
	numero_end_cliente character varying(10),
	complemento_cliente text,
	bairro_cliente text,
	cep_cliente character varying(8),
	cidade_cliente text,
	estado_cliente int,

	operadora_cliente int,

	PRIMARY KEY (id_cliente),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario),
	
	FOREIGN KEY (sexo_cliente) REFERENCES cad_item_grupo (id_item_grupo), /*Grupo 1*/
	FOREIGN KEY (pagamento_cliente) REFERENCES cad_item_grupo (id_item_grupo), /*Grupo 2*/
	FOREIGN KEY (operadora_cliente) REFERENCES cad_item_grupo (id_item_grupo), /*Grupo 3*/
	FOREIGN KEY (estado_cliente) REFERENCES cad_item_grupo (id_item_grupo), /*Grupo 4*/
	FOREIGN KEY (fk_status) REFERENCES cad_item_grupo (id_item_grupo) /*Grupo 7*/

);

CREATE TABLE IF NOT EXISTS cad_motorista(

	id_motorista int not null AUTO_INCREMENT,
	fk_usuario int,
	sexo_motorista int,
	cpf_motorista character varying(11),
	identidade_motorista character varying(10),
	pagamento_motorista int,
	fk_status int DEFAULT 45,

	rua_motorista text,
	numero_end_motorista character varying(10),
	complemento_motorista text,
	bairro_motorista text,
	cep_motorista character varying(8),
	cidade_motorista text,
	estado_motorista int,

	cnh_num_motorista character varying(20),
	cnh_cat_motorista int,
	cnh_validade date,

	operadora_motorista int,
	
	hora_localizacao timestamp null,
	latitude_localizacao float(10,6),
	longitude_localizacao float(10,6),

	PRIMARY KEY (id_motorista),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario),
	
	FOREIGN KEY (sexo_motorista) REFERENCES cad_item_grupo (id_item_grupo), 	 /*Grupo 1*/
	FOREIGN KEY (pagamento_motorista) REFERENCES cad_item_grupo (id_item_grupo), /*Grupo 2*/
	FOREIGN KEY (operadora_motorista) REFERENCES cad_item_grupo (id_item_grupo), /*Grupo 3*/
	FOREIGN KEY (estado_motorista) REFERENCES cad_item_grupo (id_item_grupo), 	 /*Grupo 4*/
	FOREIGN KEY (cnh_cat_motorista) REFERENCES cad_item_grupo (id_item_grupo), 	 /*Grupo 5*/
	FOREIGN KEY (fk_status) REFERENCES cad_item_grupo (id_item_grupo) 	 		 /*Grupo 6*/
);

CREATE TABLE IF NOT EXISTS rel_carro_motorista(

	id_carro_motorista int not null AUTO_INCREMENT,
	fk_motorista int,
	fk_tipo_carro int,
	placa_carro character varying(7),
	marca_carro text,
	modelo_carro text,
	ano int,
	seguro_app boolean,
	validade_seguro date,
	seguro_carga boolean,

	PRIMARY KEY (id_carro_motorista),
	FOREIGN KEY (fk_motorista) REFERENCES cad_cliente (id_cliente),
	FOREIGN KEY (fk_motorista) REFERENCES cad_cliente (id_cliente),
	FOREIGN KEY (fk_tipo_carro) REFERENCES cad_tipo_carro (id_tipo_carro)



);

/*Telas*/
-- Config
INSERT INTO seg_models (id_model,link_model, descricao_model) VALUES (7,'Model_config', 'Model Config, mensagens');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (6,'Controller_config', 'Controller Config, mensagens', 7);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (15,'config/view_mensagens', 'Mensagens enviadas', 'Lista das mensagens', 6);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(15,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (16,'config/view_tarifas', 'Tarifas', 'Lista das tarifas', 6);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(16,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (17,'relatorios/view_faturamento', 'Faturamento', 'Relatório de faturamento', 5);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(17,1);

-- Clientes
INSERT INTO seg_models (id_model,link_model, descricao_model) VALUES (8,'Model_clientes', 'Model clientes');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (7,'Controller_clientes', 'Controller clientes', 8);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (18,'clientes/view_clientes', 'Clientes', 'Relatório de Clientes', 7);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(18,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (19,'clientes/view_novo_cliente', 'Cadastro Clientes', 'Cadastro de Clientes', 7);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(19,1);

-- Motoristas
INSERT INTO seg_models (id_model,link_model, descricao_model) VALUES (9,'Model_motoristas', 'Model Motoristas');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (8,'Controller_Motoristas', 'Controller Motoristas', 9);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (20,'motoristas/view_motoristas', 'Motoristas', 'Relatório de Motoristas', 8);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(20,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (21,'motoristas/view_novo_motorista', 'Cadastro Motoristas', 'Cadastro de Motoristas', 8);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(21,1);

-- Corridas
INSERT INTO seg_models (id_model,link_model, descricao_model) VALUES (10,'Model_corridas', 'Model corridas');
INSERT INTO seg_controllers (id_controller,link_controller, descricao_controller, fk_model) VALUES (9,'Controller_corridas', 'Controller corridas', 10);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (22,'corridas/view_corridas', 'corridas', 'Lista corridas', 9);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(22,1);

INSERT INTO seg_aplicacao (id_aplicacao,link_aplicacao, titulo_aplicacao, descricao_aplicacao, fk_controller) VALUES (23,'corridas/view_relatorio_corrida', 'Relatório corridas', 'Relatório das corridas', 9);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo) values(23,1);

CREATE TABLE cad_montadoras (
	id_montadora INT NOT NULL AUTO_INCREMENT,
	montadora varchar(45) NOT NULL,
	CONSTRAINT cad_montadora_pk PRIMARY KEY (id_montadora)
);

CREATE TABLE cad_modelos (
	id_modelo INT NOT NULL AUTO_INCREMENT,
	fk_montadora INT NOT NULL,
	modelo varchar(100) NOT NULL,
	CONSTRAINT cad_modelo_pk PRIMARY KEY (id_modelo),
	CONSTRAINT cad_modelo_cad_montadora_fk FOREIGN KEY (fk_montadora) REFERENCES cad_montadoras(id_montadora)
);

CREATE TABLE cad_veiculos (
	id_veiculo INT NOT NULL AUTO_INCREMENT,
	fk_modelo INT NOT NULL,
	fk_motorista INT NOT NULL,
	cor_veiculo varchar(15) NULL,
	ano_fabricacao INT NOT NULL,
	placa_veiculo varchar(7) NOT NULL,
	fk_tipo INT NOT NULL,
	CONSTRAINT cad_veiculo_pk PRIMARY KEY (id_veiculo),
	CONSTRAINT cad_veiculo_cad_modelo_fk FOREIGN KEY (fk_modelo) REFERENCES cad_modelos(id_modelo),
	CONSTRAINT cad_veiculo_cad_motorista_fk FOREIGN KEY (fk_motorista) REFERENCES cad_motorista(id_motorista),
	CONSTRAINT cad_veiculo_cad_tipo_carro_fk FOREIGN KEY (fk_tipo) REFERENCES cad_tipo_carro(id_tipo_carro)
);

CREATE TABLE cad_seguros (
	id_seguro INT NOT NULL AUTO_INCREMENT,
	apolice_seguro varchar(100) NOT NULL,
	validade_seguro DATE NOT NULL,
	fk_veiculo INT NOT NULL,
	CONSTRAINT cad_seguro_pk PRIMARY KEY (id_seguro),
	CONSTRAINT cad_seguro_cad_veiculo_fk FOREIGN KEY (fk_veiculo) REFERENCES cad_veiculos(id_veiculo)
);

CREATE OR REPLACE VIEW view_lista_motoristas
AS select
	cmot.id_motorista id,
	su.nome_usuario nome,
	tipo.tipo_carro categoria,
	cvei.placa_veiculo placa,
	cmod.modelo veiculo,
	cig.nome_item_grupo status
	from cad_motorista cmot
	left join seg_usuarios su on (cmot.fk_usuario = su.id_usuario)
	left join cad_veiculos cvei on (cmot.id_motorista = cvei.fk_motorista)
	left join cad_modelos cmod on (cvei.fk_modelo = cmod.id_modelo)
	left join cad_tipo_carro tipo on (cvei.fk_tipo = tipo.id_tipo_carro)
	left join cad_item_grupo cig on (cmot.fk_status = cig.id_item_grupo and cig.fk_grupo = 6
);

CREATE TABLE cad_corridas (
	id_corrida INT NULL AUTO_INCREMENT,
	fk_veiculo INT NOT NULL,
	fk_cliente INT NOT NULL,
	endereco_origem varchar(255) NOT NULL,
	latitude_origem FLOAT(10,6) NOT NULL,
	longitude FLOAT(10,6) NOT NULL,
	endereco_destino varchar(255) NOT NULL,
	latitude_destino FLOAT(10,6) NOT NULL,
	longitude_destino FLOAT(10,6) NOT NULL,
	hora_inicio TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	hora_fim TIMESTAMP NULL,
	fk_status int,

	CONSTRAINT cad_corridas_pk PRIMARY KEY (id_corrida),
	CONSTRAINT cad_corridas_cad_veiculos_fk FOREIGN KEY (fk_veiculo) REFERENCES cad_veiculos(id_veiculo),
	CONSTRAINT cad_corridas_cad_cliente_fk FOREIGN KEY (fk_cliente) REFERENCES cad_cliente(id_cliente),
	FOREIGN KEY (fk_status) REFERENCES cad_item_grupo (id_item_grupo) /*Grupo 8*/
);

/* ******************************* */
/*Notificações PUSH, Tokens.*/
CREATE TABLE IF NOT EXISTS cad_tokens(

	id_token int not null auto_increment,
	token character varying(300),
	fk_usuario int,
	aparelho int, /*1 iOS, 2 Android, 3 WEB*/
	data_registro timestamp DEFAULT CURRENT_TIMESTAMP,

	PRIMARY KEY (id_token),
	FOREIGN KEY (fk_usuario) 
        REFERENCES seg_usuarios(id_usuario)

);

/*Funções*/
/*Geolocalização, calcula distancia entre dois pontos em kilometros.*/
delimiter //
CREATE FUNCTION Geo(lat_ini DOUBLE(18,10), lon_ini DOUBLE(18,10),lat_fim DOUBLE(18,10), lon_fim DOUBLE(18,10))
RETURNS DOUBLE(18,10)
NOT DETERMINISTIC
BEGIN
DECLARE Theta DOUBLE(18,10);
DECLARE Dist DOUBLE(18,10);
DECLARE Miles DOUBLE(18,10);
DECLARE kilometers DOUBLE(18,10);

SET Theta = lon_ini - lon_fim;
SET Dist  = SIN(RADIANS(lat_ini)) * SIN(RADIANS(lat_fim)) +  COS(RADIANS(lat_ini)) * COS(RADIANS(lat_fim)) * COS(RADIANS(Theta));
SET Dist  = ACOS(Dist);
SET Dist  = DEGREES(Dist);
SET Miles = Dist * 60 * 1.1515;
SET kilometers = Miles * 1.609344;

RETURN kilometers;

END;//
delimiter ;
